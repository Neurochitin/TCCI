# TCCI

TCCI (Terraria Content Creator Integration) is a mod for Terraria that lets streamers give their viewers control over the game they're playing. It is conceptually inspired by the [similarly named Minecraft mod](https://www.curseforge.com/minecraft/mc-mods/content-creator-integration), but quite different in implementation.

TCCI connects to a streaming service (currently, it supports [Streamlabs](https://streamlabs.com/) and Twitch chat) and listens to *events* like chat commands, monetary donations, bit donations, subscriptions, and some others. It then lets you define *scripts* that will be executed when an event occurs. The scripts are written in C# code, of which it is useful to know the basics when using this mod. This way, you can do things like:

* have a chat command that gives you items,
* make certain money donations spawn bosses,
* teleport you around the world when someone subscribes,

and much more. Really, the only limit is your imagination (and for complex ideas, your programming skills); theoretically, TCCI scripts have access to everything C# code has to offer.

This leads to a **VERY IMPORTANT NOTICE: in theory, scripts can do whatever they want on your computer!** This includes deleting important files, sending secrets over the network, whatever you can imagine. TCCI includes some basic measures to prevent obvious malware, but the measures are not foolproof (or you can be tricked into disabling them). If you write all the scripts yourself, this should not be a problem, but **be careful when using scripts from other people**!

Also, the same general disclaimer when using CCI for Minecraft applies: TCCI is a tool that can/will affect your stream, for better or for worse! It entirely depends on what you do with it; TCCI can't be held liable for this.

A note on this readme: as it is TCCI's only documentation, it should cover all of its features. It serves as a guide, a reference, and an example collection all in one. (It is written as a guide, with reference and example sections placed at the relevant spots.) If you think something is missing, let me know.

If you need help, you can contact me on Discord by joining [TCCI's server](https://discord.gg/pnveUJW), or messaging me directly at Neurochitin#0995.

Have fun!

## Getting the mod

See TCCI's [releases page](https://gitlab.com/Neurochitin/TCCI/-/releases) for binary downloads. However, note that it is an alpha version — it might not work at all!

You can of course build it from source yourself. TCCI has one external dependency, namely [IrcDotNet](https://github.com/IrcDotNet/IrcDotNet) version **0.6.1**. If you obtain a DLL of that and place it in the lib/ folder as usual, you should be able to use tML to compile and use the mod.

## Terminology

Some general terminology that is important to keep in mind for understanding this documentation:

* An *event* is something that a stream viewer does (for example, donating $5). In general, an event has a *type* (in this case "donation") and a *parameter* (in this case "5"). It also has additional *info* associated with it, containing for example the name of the user who donated, or the donation message.
* A *script* is a piece of code that runs on your computer. Every script has a *name* that uniquely defines what events it is triggered by (for example `donation5`). Every script is also one of three types: a *specific* script that is specific to both event type and event parameter (i.e. it would only be triggered by a $5 donation, not a $3 donation), a *generic* script that is only specific to event type (it would be triggered by donations of any amount, but not by other events), and an *"always"* script that is triggered by any vent.
* A *service* is a place where events occur (i.e. Streamlabs or Twitch chat).
* More terminology goes here.
* "tML" is tModLoader, SL is Streamlabs.

Also, I will occasionally refer to the Minecraft mod "Content Creator Integration", which this mod is conceptually inspired by, in order to highlight important differences. To differentiate it from TCCI, I will call the Minecraft mod *"MCCI"*. However, I have never actually used MCCI myself, I only watched some streamers using it and I read its documentation. So what I'm saying might actually be totally wrong.

## Basic usage

Note: TCCI has mostly been developed with singleplayer in mind. It also works fine on multiplayer, but the configuration process is a little different and you will have to use different scripts some of the time. From here on, it is assumed that you are playing in singleplayer; occasionally, important differences in multiplayer games will be mentioned when those exist.

Once you have installed, enabled, and loaded TCCI into your tML instance, you must first configure it. Open TCCI's config in the in-game mod configuration screen and configure these important things:

### Important config fields

- **TwitchChannel**: The Twitch channel whose chat TCCI should connect to. Simply enter your Twitch channel name here (e.g. "neurochitin"). TCCI will connect to the chat anonymously, so no authentication is required here.
- **CommandPrefix**: The prefix that should be in front of commands. For example, if you want your viewers to be able to use an "!eye" command that spawns the Eye of Cthulhu, the CommandPrefix would have to be `!`, which is the default value (as it is the norm on Twitch that commands have the `!` prefix).
- **StreamlabsToken**: Your token for the Streamlabs socket API. You can find this by opening your Streamlabs dashboard, going to Settings (bottom left) -> API Settings -> API Tokens, then showing "Your Socket API Token". Because it is quite long, it is probably not a good idea to enter this entirely in Terraria itself: you can also set this field to some dummy value within Terraria so it shows up in the JSON file, then close the game, open the TCCI config JSON file (can be found under `Terraria/ModLoader/Mod Configs/TCCI_TCCIConfig.json`), copy and paste the value from your browser into the file (replacing the dummy value), saving the file, and reopening the game.

### Not so important config fields, in alphabetical order

- "Anti virus" enabled: enables or disables the checks that are performed on script files to try to detect malicious files. If you are writing really advanced scripts that make use of reflection or file system stuff, you might have to disable this. But don't disable it just because someone tells you to!
- ExtraAssemblyPaths: paths to additional DLL files that scripts will be compiled against. This is useful if you are writing a script that uses unusual functionality, like network access. But the same note as for the AntiVirus applies here: if someone tells you to add anything here, they might be trying to trick you!
- IRC connection timeout: when connecting to Twitch chat, if the server doesn't respond after this amount of milliseconds, the connection will be considered to have failed.
- Show TCCI status emblem: if this is enabled, a small emblem will be shown below the player's buffs that changes in colour corresponding to the current connection status and the number of loaded scripts. If there is at least one script loaded and at least one connection to a service established, it will turn green. Hovering over it also lets you view the number of loaded scripts as well as which connections are present. 
- Start paused: whether the execution of scripts in response to events is paused by default when opening the game. Off by default because the execution requires the scripts to have been manually loaded anyway.

### The TCCI command
 
Now that you have configured TCCI, you can start actually using it. Open any singleplayer world. Most aspects of TCCI are controlled using the **`/tcci`** command.

### TCCI command reference

All TCCI subcommands (as well as the `/tcci` command itself) are case insensitive. `/TCCI testScript` will work just as well as `/tcci testscript` or `/TCcI TEstScRiPT`. For consistency, I will use camelCase to refer to subcommands here.

* `/tcci connect [chat|streamlabs|all]`: Connects to Twitch chat, Streamlabs, or both. If the connection was successful, a "Connected to <X>" message should show up in chat. Note that, unlike in MCCI where it appears to be common practice to only connect to the services once you actually intend events to be processed (for example, you could start your game, then give yourself a grace period where chat can't do anything, then after that has passed you connect to the services): in TCCI it is preferable to connect to the services immediately when opening the world, then using `/tcci pause` and `/tcci resume` to control event processing independently from connections.
* `/tcci disconnect [chat|streamlabs|all]`: Disconnect from one or all services.
* `/tcci pause`: Pause event processing and script execution independently from connections to services.
* `/tcci resume`: Resume event processing.
* `/tcci viewOutput <scriptName>`: View the output produced while compiling the script with the given name.
* `/tcci testScript <scriptName> [field1=value1,field2=value2]`: Run the given script to test its effects. If the script doesn't exist, an error will be produced. Optionally, you can specify parameters for the script, which causes the respective `EventInfo` fields to be filled with the information. This is very useful when testing generic scripts, like `/tcci testScript bits Amount=1000`. An alternative to specifying this every time is to use Streamlabs' test events (see "Testing scripts" below).
* `/tcci reload`: Reload all scripts. This will recompile all changed scripts, as well as all other scripts if the environment has changed (more on that below). Note that the compilation might take a few seconds per script, which sums up to a long time if there are a lot of scripts! You can do other things while this is running.
* `/tcci cleanReload`: Reload and recompile all scripts independently of the environment. Do this if you get mysterious compilation errors.
* `/tcci setCooldown <scriptName> <seconds>`: Sets the cooldown period of the given script to some number of seconds. Will also work if the script does not exist (although it will return a warning) — in this case it will apply to future scripts of that name. Note that this does not reset currently active cooldowns, it only lengthens or shortens them depending on the value: if you have a command that previously had a cooldown of 60 seconds and that has last been used 30 seconds ago, and you set its cooldown to 120 seconds, you will now have to wait 90 seconds until next usage.
* `/tcci getCooldown <scriptName>`: Gets the cooldown period of the given script, as well as the time until the next possible execution if it is currently on cooldown.
* `/tcci toggleCooldowns`: Toggle whether cooldowns are observed or not. Note that re-enabling cooldowns with this command will not reset cooldowns, which has the consequence that if a command that usually has a cooldown has been used in a period where cooldowns were disabled and you re-enable cooldowns, you will now have to wait until the last usage of the command will have been more than the cooldown period in the past.
* `/tcci resetCooldown <scriptName>`: Resets cooldown of the given script, which means it can be used again if it was previously on cooldown.
* `/tcci replay`: Replays the event that occurred last.
* `/tcci targetme`: Sets you as the default player to target. This will not have any effect in singleplayer or if there is only one player online, but if there are multiple players online, the `Player` utility property will always refer to you after using this command, which means scripts that are coded without a specific player in mind will act on you.

Also, for quick reloading and testing, TCCI by default registers the `/r` command as an alias for `/tcci reload`, as well as the `/t <name>` command for `/tcci testscript <name>`. For even more convenience, `/t` run without any arguments will re-run the script that was last tested using `/t <name>`.

### Connecting

Use `/tcci connect all` to connect to both Streamlabs and Twitch chat. If all goes well, you should get two messages, telling you that you succeeded in connecting to both services.

This is an optional step, if you don't want to connect to the services right away and instead want to start with writing scripts, that is also possible; the two things are independent from each other.

## Scripting

By far the most complex and daunting, but also the most crucial, part of TCCI is the scripting support. Scripts are small snippets of C# code that have access to all of Terraria's code and can do pretty much whatever they want.

### Script organisation

Scripts are stored in the `Terraria/ModLoader/TCCI Scripts` folder. Each script is a file with the `.csx` (C# script) extension.

#### The template script

To reduce verbosity, scripts are only fragments of C# code that are inserted into a template script file. This template script file is automatically generated at first mod load (and regenerated when deleted) and can be found at `TCCI Scripts/template.csx`.

The template file contains two directives that are replaced with actual data: `SCRIPTNAME` will be replaced with the name of the script, and `/* CODE */` will be replaced with its code.

Usually, there is not much reason for you to edit the template file. However, the possibility is there should the need arise.

#### Script names

The name of a script is the filename before the extension. For example, if the script file is called `bits400.csx`, the script's name would be `bits400`. **Crucially, the script name determines exactly which events the script will be executed for**; there is no other mapping step where scripts are associated to events.

The script name contains of two parts: the event type (in the above case `bits`) and the parameter to match (`400`). There is no separator between the two parts; TCCI is smart enough to figure out where the type ends and the parameter begins.

For every event type, the parameter may also be omitted to create a generic script that will be executed for all events of a given type. For example, the script name `bits` (resulting in the filename `bits.csx`) will be executed every time someone donates *any* amount of bits, allowing for more complex script logic.

Finally, the special script with the name `always` will *always* be executed when an event occurs, no matter the type or parameter.

#### Event types

Scripts can be created to listen to the following event types:

* **`command`**: a command in chat (a chat message that starts with the command prefix specified in TCCI's configuration). The parameter for this event is the name of the command. For example, a script like `commandTest` (or even `commandtest`; the command name is case insensitive) would be executed if someone writes "!test" in chat. Notably, only the first part of the chat message is considered for this; a message like "!test is a great command" would also cause the `commandTest` script to be executed. A generic script with the name `command` will listen to all commands.
* **`follow`**: triggered when someone follows the channel. Has no parameter.
* **`subscription`**: triggered when someone subscribes to the channel for a certain amount of months. The amount of months is the parameter: `subscription12` would only be triggered when someone subscribes for twelve months at once. This event should also be triggered when gift subs are donated. (TODO: find out how to differentiate the two cases)
* **`resub`**: triggered when someone resubscribes for a certain amount of months. The new amount of subscribed months is the parameter. TODO: elaborate what resub actually means in detail once I find it out myself
* **`donation`**: triggered when someone donates an amount of money directly (i.e. not using bits). The amount of money is the parameter. Only integer amounts of money are supported: `donation3.14` would be an invalid script name, as opposed to `donation3`, which would be valid. Fractional amounts should be rounded to the correct integer amount (TODO: find out whether this actually happens). TODO: Find out what happens with donations in other currencies than USD, do they get converted or is the amount in the other currency used directly?
* **`bits`**: triggered when someone sends a message containing bits. The parameter is the amount of bits in the message.
* **`host`**: triggered when someone hosts the channel. The parameter is the amount of viewers that are watching via the hosting channel, although I imagine there are few situations where a specific script for hosting for a certain amount of viewers are useful. So creating a generic script is recommended here.
* **`raid`**: triggered when someone raids the channel. The parameter is the amount of viewers, as for `host`.
* **`merch`**: triggered when someone purchases merch via SL. This event type has no parameter.
* **`loyalty_store_redemption`**: triggered when someone redeems something in the SL loyalty store. (TODO: find out what that actually means) No parameter.
* **`prime_sub_gift`**: triggered when someone donates a gift of Streamlabs Prime. Note that this is not the same thing as Twitch gift subs, nor Twitch Prime subs! No parameter.

### Your first script

To test out scripting, create a script with a valid filename (for example `commandTest.csx`) in the `TCCI Scripts` folder. Fill it with some code. A simple choice is the following example, that just writes a message to chat:

```c#
ChatMessage("Hello world!");
```

### Reloading

Before you can try out the script, you will have to load it into memory. Use `/tcci reload` to reload all scripts from the scripts folder. **Important: you have to do this every time TCCI is loaded** (i.e. after every game startup, or every time you change which mods you're running).

The reason for this is that the scripts have to be *compiled* from a human-writable code form into a machine-executable *assembly* form. This compilation step has two important properties:

* it takes a comparatively long time, up to a few seconds per script. This may not seem like a lot, but if you have a lot of scripts, it quickly adds up!
* it is dependent on the *environment*: the set of loaded mods, including not only their versions, but also their exact assembly data. This means that if a mod is rebuilt unchanged, all scripts will also have to be recompiled. (TODO: implement and elaborate how this may not apply to all mods)

TCCI stores compiled assemblies for each script, so most of the time intensive compilation can be skipped on most reloads, enabling quick recompilation and testing of changed scripts. To force all scripts to be recompiled, use `/tcci cleanReload`.

When you use `/tcci reload` or `/tcci cleanReload`, a progress bar will be shown to the right of the hotbar that displays the progress with reloading scripts. Currently, this is only implemented in singleplayer.

### Testing the script

After the reload is finished, you can try out the script by using `/tcci testScript commandTest` (or whatever name you gave the script). If all goes well, you should see a chat message!

You can also connect to Twitch chat (`/tcci connect chat`) and try using the `!test` command directly in chat. The same outcome should occur.

If, rather than a command script, you wrote a script that listens to a Streamlabs event, you can test it in the same way using `/tcci testScript`. If your script accesses the event info in any way, you might want to specify values that should be set there. This is done by appending a list of fields and values in the `field1=value1,field2=value2` syntax, for example: `/tcci testScript bits amount=1000`

Other than this, you can also use Streamlabs' test events to more accurately simulate what would happen in an actual stream: Connect to Streamlabs and navigate to the the "Alert Box" section on your SL dashboard, then simply click one of the "Test <Event>" buttons; the SL widget does not need to be running for this to trigger an event with TCCI. However, note that this method generates events with mostly random parameters — for example, bit and donation test events will have amounts of money randomly selected from some possible values. So this method is more suitable for testing generic events.

## Scripting reference

Now to start with writing actual functional scripts. A good base to go off of is TCCI's collection of example scripts found here https://gitlab.com/Neurochitin/TCCI-script-collection. However, in the following section there will be some documentation on TCCI's utility functions and some basic Terraria functions that should enable you to write really basic scripts by yourself.

If you want to write really complex scripts that do obscure things in Terraria, there is no way around learning the basics (and the advanced stuff) of Terraria modding. A comprehensive documentation for that doesn't really exist, but [tModLoader's wiki](https://github.com/tModLoader/tModLoader/wiki) is not so bad. Once you reach the limits of that, you will have to look at the source code of Terraria or other mods, to see how they do things.

As noted above, TCCI provides some utility functions that make doing certain things more convenient. The goal is to hide C#-specific idiosyncrasies from people who just want to write some really basic scripts without knowing C# very well. However, be expected to have to learn how to use C# once you reach a certain point.

The source code of all the utility functions can be found [here](https://gitlab.com/Neurochitin/TCCI/-/blob/master/Scripting/UtilityMethods.cs). The general rule is that you are using something Terraria-specific if you are calling a method on a specific class or object (e.g. `Player.QuickSpawnItem`) and something TCCI-specific if you are just calling a method without specifying an object (e.g. `SpawnNPCAtPlayer` or `PlaceTileAtOffset`).

### A note on coordinates

Terraria has two types of coordinates: tile coordinates and entity coordinates. For both types of coordinates, the **positive x direction is to the right, and the positive y direction is to the bottom**. That means that for both coordinates, (0,0) is the upper left corner of the map.

Tile coordinates are in units of one tile. If the tile coordinate of something is +3 in the x direction compared to something else, it is 3 tiles to its right, as you would expect. Entity coordinates are scaled down by a factor of 16: one tile is 16x16 entity coordinates. If the entity coordinate of something is +48 in the x direction, it will be 3 tiles to the right.

In my opinion, it is kind of stupid to have two types of coordinates. So **all TCCI methods will use tile coordinates**. If TCCI asks you to specify a coordinate, just enter a number of tiles and it will be what you expect. However, it is important to be aware of this difference once you start using Terraria's own methods, because Terraria uses entity coordinates when referring to entities (NPCs, dusts, projectiles), and tile coordinates when referring to tiles.

### NPCs

An NPC is **any character that is not a player**. This includes not only town NPCs, but also all enemies, critters, and so on.

#### Spawning an NPC

To quickly spawn an NPC, use `SpawnNPCAtPlayer`:

```c#
SpawnNPCAtPlayer(1);
```

This will spawn a blue slime at the player's position. The `1` is the ID of the blue slime NPC. Find a list of all NPCs with their IDs [here](https://github.com/tModLoader/tModLoader/wiki/Vanilla-NPC-IDs).

Sometimes you want to spawn an NPC not exactly at the player's position, but at an offset. The method also supports this:

```c#
SpawnNPCAtPlayer(1, 0, -10);
```

This will spawn a blue slime 0 tiles in the x direction and -10 tiles in the y direction (i.e. 10 tiles above where the player is).

#### Modifying properties

The `SpawnNPCAtPlayer` method also returns an `NPC` object that can be stored in a variable. Then, you can modify its properties after spawning:

```c#
var spawnedSlime = SpawnNPCAtPlayer(1);
spawnedSlime.GivenName = "Slimey";
spawnedSlime.life = 200;
spawnedSlime.lifeMax = 200;
spawnedSlime.damage *= 2;
```

This will spawn a slime with the name "Slimey" and 200 max (and current) health, that deals twice the amount of damage as usual. For the things you can modify, see [here](https://github.com/tModLoader/tModLoader/wiki/NPC-Class-Documentation#fields-and-properties) for a reference and [here](https://docs.google.com/spreadsheets/d/1y5_y1ptnlcPhcxh6jCOsMApX1W6FI3l8duHtEaLqkro/edit#gid=1969633944) for a table of all NPCs with their default field values.

#### Multi-part enemies

Multi-part enemies are enemies that consist of multiple parts that move independently from each other to a certain degree. This includes worms, where each worm segment is one part, or bosses that have multiple parts that can move separately (Skeletron, Skeletron Prime, the Moon Lord, ...) Multi-part enemies work such that one part is the "main" part that all other parts refer to (with worms, this is the head part for example). This main part is spawned on its own, and as part of its AI code, it will spawn the other parts.

To spawn such a multi-part enemy, simply find the ID of the main part (worm head, Moon Lord Core, Skeletron Head, ...) and spawn it like above. Note that modifying properties will not work well on multi-part NPCs because the other spawned parts will not have the same properties.

#### Existing NPCs

You can use the `ForAllNPCsWithin` method to do something on all NPCs in a certain radius within the player:

```c#
ForAllNPCsWithin(20, npc => {
    npc.scale *= 2;
});
```

This will make all NPCs within 20 tiles of the player become twice as big visually. (To also affect their hitbox, you could use `npc.Size *= 2;` as well.)

### The player

TCCI exposes a property `Player` which refers to a player currently playing in the world. This is most useful in singleplayer, where there is of course only one player in the world, which the `Player` property will refer to. In multiplayer, the `Player` property will refer to the first player by ID. This is guaranteed not to change unless somebody joins or leaves, so it can (mostly) be used as if it were deterministic. However, this is not very useful most of the time, as usually you will want commands to perform an action on a specific player, not just any player.

To change which player `Player` refers to if there are multiple players online, you can use the `/tcci targetme` command or set the `TargetedPlayer` utility field.

TCCI also exposes a `Players` array which contains all players currently online. It can be used to select a player by a specific criterion, to choose a random player using `RandomlySelect(Players)`, or to perform an action on all players.

If no player is online, the `Player` property will refer to the "player" with ID 0, which will not contain any useful data, but accessing fields and calling methods on it should not cause `NullPointerException`s at least.

#### Giving items

To give the player an item, it is usually simplest to spawn an item right on top of the player, so they will pick it up immediately if their inventory isn't full. (Otherwise, it will drop to the ground like any other item.) This is done like this:

```c#
Player.QuickSpawnItem(1234);
```

This will give the player one Chlorophyte Warhammer (ID 1234). A list of all item IDs can be found [here](https://github.com/tModLoader/tModLoader/wiki/Vanilla-Item-IDs). It is also possible to spawn a stack of items at once:

```c#
Player.QuickSpawnItem(74, 999);
```

This will give the player 999 platinum coins (ID 74). Just like with NPCs, you can also modify item properties. This is only done in a very limited way (reforges) in vanilla Terraria, but there are a lot more possibilities than you might be used to. To give a player a modified item, you would use `Player.QuickSpawnClonedItem` with a modified `Item` object. This is done in the following code:

```c#
var item = new Item(); // create an empty item
item.SetDefaults(1234); // initialise item as a default Chlorophyte Warhammer
item.useTime = 7; // Set use time to 7
Player.QuickSpawnClonedItem(item); // give item to player
```

This will give the player a Chlorophyte Warhammer that has a use time of 7 compared to the default 14, in effect doubling its swing speed. To see all the item properties that can be modified, look [here](https://github.com/tModLoader/tModLoader/wiki/Item-Class-Documentation); to see all items with their default values for all properties, take a look at the table [here](https://docs.google.com/spreadsheets/d/1ZsvOQeKn9nJbTw2SH77f9VmaHtxa2uqmO2zthwiV-9A/edit#gid=310625782).

A final tip: hearts (ID 58) and mana stars (ID 184), as well as the nebula armour boosts and some other things, are also items that can be spawned using the above method (`QuickSpawnItem`) — they just can't exist in an inventory.

#### Inventory

TCCI exposes an `Inventory` property that is an array of items including the player's entire inventory (main items, held item, armour, vanity, dyes, accessories, equipment). It is useful for iterating over, if you want to do something to all the player's items. Otherwise it is usually more useful to refer to the specific arrays in `Player`:

* `Player.inventory` includes the hotbar (items 0-9), the first, second, third, and fourth inventory rows below the hotbar (items 10-19, 20-29, 30-39, and 40-49 respectively), coins (50-53), ammo (54-57), and the item held by the cursor in the inventory screen (58). So to refer to the first item in the ammo slots for example, you could do `Player.inventory[54]`.
* `Player.armor` includes the player's main armour (0-2) and accessories (3-9), as well as vanity armour (10-12) and vanity accessories (13-19)
* `Player.dye` includes the dye items for armour (0-2) and accessories (3-9).
* `Player.miscEquips` includes the player's equipment: pet (0), light pet (1), minecart (2), mount (3) and hook (4).
* `Player.miscDyes` includes the dye items for the equipment, in the same order as above.

To delete an item, set it to `new Item()`:

```c#
Player.armor[1] = new Item();
```

The above code would delete the item in the player's chestplate slot. To modify an item, you can create one (like above) and set the respective slot to the item:

```c#
var item = new Item();
item.SetDefaults(2771);
Player.miscEquips[3] = item;
```

This would set the player's mount item to Brain Scrambler (Scutlix mount). As before, you could also modify the item's properties before setting the respective inventory slot to it. You can also modify properties in place without creating a new item:

```c#
Player.armor[1].defense += 10;
```

This would make the player's chestplate have 10 more defense than before. Note that this does not change the item's prefix! Changing prefixes (i.e. reforging) is a different process that also sets item properties.

### Projectiles

### Tiles

### Randomness

### Miscellaneous

## Troubleshooting

Here are some errors that might occur for you:

### Corlib not in sync with this runtime: expected corlib string [...]

Try updating tModLoader's development environment.

- Note on casting errors here