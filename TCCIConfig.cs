using System.Collections.Generic;
using System.ComponentModel;
using Terraria.ModLoader.Config;

namespace TCCI
{
    public class TCCIConfig : ModConfig
    {
        [DefaultValue(true)]
        [Label("\"Anti virus\" enabled")]
        [Description(
            "Enables some basic checks that are performed against script code, to prevent some straightforward kinds of malware scripts.")]
        public bool AntiVirusEnabled;

        [DefaultValue("!")] public string CommandPrefix;

        public List<string> ExtraAssemblyPaths = new List<string>();

        [DefaultValue(10000)] [Label("IRC connection timeout (ms)")]
        public int IrcConnectionTimeout;

        [DefaultValue(false)]
        [Label("Start paused")]
        [Description(
            "If true, execution of actions will be paused initially. Use '/tcci resume' after connecting to unpause.")]
        public bool StartExecutionPaused;

        [DefaultValue(true)]
        [Label("Show TCCI status emblem")]
        [Description(
            "Controls whether or not the emblem showing TCCI status information will be displayed below the player's hotbar and buffs.")]
        public bool ShowStatusUI;

        public string StreamlabsToken;
        public string TwitchChannel;
        public override ConfigScope Mode => ConfigScope.ServerSide;
    }
}