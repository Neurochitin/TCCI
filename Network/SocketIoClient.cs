using System;
using System.Collections.Concurrent;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.WebSockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Terraria.ModLoader;

// Mostly copied from https://github.com/it9gamelog/socketio-with-ws-client
namespace TCCI.Network
{
    public class PingEventArgs : EventArgs
    {
        internal PingEventArgs(int sent, int received)
        {
            SentAtTick = sent;
            ReceivedAtTick = received;
        }

        public int SentAtTick { get; }
        public int ReceivedAtTick { get; }
    }

    public class IncomingEventEventArgs : EventArgs
    {
        private static readonly Regex eventRegex = new Regex("^(?:(/[^,]*),)?([0-9]{1,19})?(.*)$");

        internal IncomingEventEventArgs(string wirePayload)
        {
            var m = eventRegex.Match(wirePayload);
            if (m.Success)
            {
                Nsp = m.Groups[1].Success ? m.Groups[0].Value : "/";
                Id = m.Groups[2].Success
                    ? (long?) Convert.ToInt64(m.Groups[2].Value, CultureInfo.InvariantCulture)
                    : null;
                Payload = m.Groups[3].Value;
            }
            else
            {
                Payload = string.Empty;
            }
        }

        /// <summary>
        ///     Namespace
        /// </summary>
        public string Nsp { get; }

        /// <summary>
        ///     Id for ACK according to the socket.io protocol (Not handled by this)
        /// </summary>
        public long? Id { get; }

        /// <summary>
        ///     JSON Payload
        /// </summary>
        public string Payload { get; }
    }

    /// <summary>
    ///     A client for connecting to socket.io server and support the default protocol and very raw data exchange
    /// </summary>
    internal class SocketIoClient : IDisposable
    {
        public delegate void IncomingEventEventHandler(object sender, IncomingEventEventArgs e);

        public delegate void PingEventHandler(object sender, PingEventArgs e);

        private const int CREATED = 0;
        private const int CONNECTING = 1;
        private const int CONNECTED = 2;
        private const int DISPOSED = 3;
        private static readonly Random random = new Random();

        private readonly CancellationTokenSource cts;

        private ClientWebSocket innerWebSocket;

        private string originalQuery;

        /// <summary>
        ///     SenderLoop should send a ping when this is 1, and reset to 0 when it is actually sent.
        /// </summary>
        private int pingRequest;

        /// <summary>
        ///     The Environment.TickCount when ping is sent
        /// </summary>
        private int pingRequestAt;

        private int pingRequestDeadline;
        private int pongTimeoutDeadline;


        // NOTE: If needed, replace the following implementations such as with DataFlow TPL
        private readonly ConcurrentQueue<byte[]> senderQueue = new ConcurrentQueue<byte[]>();

        private int state;

        public SocketIoClient(int bufferSize = 1024 * 16)
        {
            cts = new CancellationTokenSource();
            PingInterval = TimeSpan.FromSeconds(1);
            PingTimeout = TimeSpan.FromSeconds(30);
            BufferSize = bufferSize;
            state = CREATED;
        }

        public TimeSpan PingInterval { get; set; }
        public TimeSpan PingTimeout { get; set; }
        public string SessionId { get; private set; }
        public int BufferSize { get; }

        public void Dispose()
        {
            var oldState = Interlocked.Exchange(ref state, DISPOSED);
            if (oldState == DISPOSED)
                // No cleanup required.
                return;
            if (innerWebSocket != null)
            {
                innerWebSocket.Dispose();
                WakeSenderLoop();
            }
        }

        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        ///     When a engine.io ping-pong is exchanged
        /// </summary>
        public event EventHandler<PingEventArgs> Ping;

        /// <summary>
        ///     Incoming socket.io event. Only basic event is supported now, ack, binary event and binary ack is ignored.
        /// </summary>
        public event EventHandler<IncomingEventEventArgs> IncomingEvent;

        /// <summary>
        ///     When the socket is dead after succesfully connected.
        ///     Note: connecting failure would not raise this event.
        /// </summary>
        public event EventHandler Disconnect;

        public event EventHandler Connect;


        /// <summary>
        ///     Connect to a socket.io specified by Uri. Ping settings indicated by the handshake will be used.
        /// </summary>
        /// <exception cref="WebSocketException">Every fault is wrapped in this exception</exception>
        /// <exception cref="OperationCanceledException">If cancellationToken is triggered</exception>
        /// <param name="uri">
        ///     uri of socket.io server. If no path is specified, /socket.io/ will be used. Query could be put in
        ///     Uri.Query
        /// </param>
        /// <param name="cancellationToken">Cancellation token for the task</param>
        public Task ConnectAsync(Uri uri, CancellationToken cancellationToken)
        {
            return ConnectAsync(uri, false, cancellationToken);
        }

        /// <summary>
        ///     Connect to a socket.io specified by Uri
        /// </summary>
        /// <exception cref="WebSocketException">Every fault is wrapped in this exception</exception>
        /// <exception cref="OperationCanceledException">If cancellationToken is triggered</exception>
        /// <param name="uri">
        ///     uri of socket.io server. If no path is specified, /socket.io/ will be used. Query could be put in
        ///     Uri.Query
        /// </param>
        /// <param name="ignorePingSettings">If true, ignore the suggested ping settings in handshake</param>
        /// <param name="cancellationToken">Cancellation token for the task</param>
        public async Task ConnectAsync(Uri uri, bool ignorePingSettings, CancellationToken cancellationToken)
        {
            originalQuery = uri.Query;

            var oldState = Interlocked.CompareExchange(ref state, CONNECTING, CREATED);
            if (oldState == DISPOSED)
                throw new ObjectDisposedException(GetType().FullName);
            if (oldState != CREATED)
                throw new InvalidOperationException("Socket is already started");

            // You might want to add the following before calling connect
            //   if connecting to a modern TLS v1.2 server
            // ServicePointManager.Expect100Continue = true;
            // ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var ub = new UriBuilder(uri);

            // If not otherwise specified, set path to standard /socket.io/
            if (string.IsNullOrEmpty(uri.LocalPath) || uri.LocalPath == "/")
                ub.Path = "/socket.io/";

            await ConnectHandshakeAsync(ub, ignorePingSettings, cancellationToken);
            await ConnectHandshakeFinalize(ub, cancellationToken);

            if (Interlocked.CompareExchange(ref state, CONNECTED, CONNECTING) != CONNECTING)
                throw new ObjectDisposedException(GetType().FullName);
        }

        private async Task ConnectHandshakeAsync(UriBuilder ub, bool ignorePingSettings,
            CancellationToken cancellationToken)
        {
            // Initiate a Engine IO v3, polling connection
            ub.Query = "EIO=3&transport=polling&t=" + RandomString(10) +
                       (!string.IsNullOrEmpty(ub.Query) && ub.Query.StartsWith("?")
                           ? "&" + ub.Query.Substring(1)
                           : string.Empty);

            byte[] buf;
            int mark, len = 0;
            using (var client = new HttpClient())
            {
                try
                {
                    Uri toConnect = SanitiseUri(ub.Uri);
                    ModContent.GetInstance<TCCI>().Logger.Debug("HTTP connecting to: " + DebugUri(toConnect));
                    var handshake = await client.GetAsync(toConnect, cancellationToken);
                    if (!handshake.IsSuccessStatusCode)
                        throw new WebSocketException(WebSocketError.HeaderError,
                            "Handshake response gave failure response");
                    ModContent.GetInstance<TCCI>().Logger.Debug("HTTP connected.");
                    buf = await handshake.Content.ReadAsByteArrayAsync();
                }
                catch (HttpRequestException)
                {
                    throw new WebSocketException(WebSocketError.HeaderError, "Handshake request failed");
                }
            }

            // Response is formatted according to https://github.com/socketio/engine.io-protocol, XHR2 section
            // <0 for string data, 1 for binary data><Any number of numbers between 0 and 9><The number 255><packet1 (first type, then data)>[...]
            // Example: \x00 \x04 \x00 \x02 \xff 0 "Json Data"
            //   where the first \x00 indicates string data
            //   \x04 \x00 \x02 means the following packet is 402 bytes long
            //   \xff marks the beginning of the packet
            //   Packet type 0 means "Open" command 

            // Blah blah. Streamlabs doesn't send XHR2 stuff so we use the "old" method instead
            if (buf.Length < 2 /* || buf[0] != 0x00 */)
                throw new WebSocketException(WebSocketError.HeaderError,
                    "Handshake response is not a standard engine.io string data");

            mark = Array.IndexOf(buf, (byte) /*0xff*/ 0x3a, 0, buf.Length);
            if (mark < 0)
                throw new WebSocketException(WebSocketError.HeaderError, "Handshake response is malformed");
            var lengthSegment = new ArraySegment<byte>(buf, /*1*/ 0, mark /*-1*/);
            /*foreach (var i in lengthSegment)
            {
                len = len * 10 + i;
            }*/
            var lengthString = Encoding.ASCII.GetString(lengthSegment.ToArray());
            len = int.Parse(lengthString);

            if (mark + len + 1 > buf.Length || len < 1)
                throw new WebSocketException(WebSocketError.HeaderError,
                    "Handshake length should not be longer than the data received");
            if (len < 1)
                throw new WebSocketException(WebSocketError.HeaderError, "Handshake length is too short");
            if (buf[mark + 1] != '0')
                throw new WebSocketException(WebSocketError.HeaderError, "Handshake should be an OPEN message");

            string responseText;
            try
            {
                responseText = Encoding.UTF8.GetString(buf, mark + 2, len - 1);
            }
            catch (ArgumentException ex)
            {
                throw new WebSocketException(WebSocketError.HeaderError, "Handshake message should be UTF-8 valid", ex);
            }

            JObject responseJson;
            try
            {
                responseJson = JObject.Parse(responseText);
            }
            catch (JsonReaderException ex)
            {
                throw new WebSocketException(WebSocketError.HeaderError, "Handshake message should be an JSON", ex);
            }

            try
            {
                /* open message: https://github.com/socketio/engine.io-protocol
                    { "sid": "RANDOM", "upgrades": ["websocket"], "pingInterval": 1000, "pingTimeout": 2000 }                    
                */
                SessionId = (string) responseJson.SelectToken("sid");
                if (string.IsNullOrWhiteSpace(SessionId))
                    throw new WebSocketException(WebSocketError.HeaderError, "Failed to extract sid in handshake");
                if (!ignorePingSettings)
                {
                    var tPingInterval = responseJson.SelectToken("pingInterval");
                    if (tPingInterval != null)
                        PingInterval = TimeSpan.FromMilliseconds((int) tPingInterval);
                    var tPingTimeout = responseJson.SelectToken("pingTimeout");
                    if (tPingTimeout != null)
                        PingTimeout = TimeSpan.FromMilliseconds((int) tPingTimeout);
                }

                var tUpgrades = responseJson["upgrades"];
                if (!(tUpgrades is JArray))
                    throw new WebSocketException(WebSocketError.HeaderError, "Failed to extract upgrades in handshake");
                var found = false;
                foreach (var e in tUpgrades)
                    if ((string) e == "websocket")
                        found = true;
                if (!found)
                    throw new WebSocketException(WebSocketError.UnsupportedVersion,
                        "Handshake said WebSocket protocol is not supported, yet we don't support other protocol");
            }
            catch (FormatException ex)
            {
                throw new WebSocketException(WebSocketError.HeaderError, "Failed to parse handshake message", ex);
            }
        }

        private async Task ConnectHandshakeFinalize(UriBuilder ub, CancellationToken cancellationToken)
        {
            // The following exchanges follows the Encoding/example in
            // https://github.com/socketio/engine.io-protocol/blob/master/README.md

            ub.Query = (originalQuery != "" ? originalQuery + "&" : "") + "EIO=3&transport=websocket&sid=" + SessionId;
            ub.Scheme = ub.Scheme == "https" ? "wss" : "ws";

            // Connect with standard WebSocket
            try
            {
                innerWebSocket = new ClientWebSocket();
                Uri toConnect = new Uri(SanitiseUri(ub.Uri).AbsoluteUri.Replace("??", "?"));
                ModContent.GetInstance<TCCI>().Logger.Debug("WSS connecting to URI: " + DebugUri(toConnect));
                await innerWebSocket.ConnectAsync(toConnect, cancellationToken);
            }
            catch (WebSocketException ex)
            {
                throw new WebSocketException("Failed to connect the websocket", ex);
            }

            // Send "2probe"
            var buffer = new byte[512];
            var bufferSegment = new ArraySegment<byte>(buffer);
            try
            {
                // engine.io<Ping(2)>
                await innerWebSocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes("2probe")),
                    WebSocketMessageType.Text, true, cancellationToken);
            }
            catch (WebSocketException ex)
            {
                throw new WebSocketException("Failed to send probe", ex);
            }

            // Get "3probe"
            WebSocketReceiveResult r;
            try
            {
                r = await innerWebSocket.ReceiveAsync(bufferSegment, CancellationToken.None);
            }
            catch (WebSocketException ex)
            {
                throw new WebSocketException("Failed to receive probe or send upgrade command", ex);
            }

            do
            {
                if (r.MessageType == WebSocketMessageType.Text)
                {
                    var t = Encoding.UTF8.GetString(bufferSegment.Array, bufferSegment.Offset, r.Count);
                    if (t.StartsWith("3")) // Server should send "3probe", yet anything starts with "3" is ok
                    {
                        // Got engine.io<Pong(3)>
                        // Sending engine.io<Upgrade(5)>
                        await innerWebSocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes("5")),
                            WebSocketMessageType.Text, true, cancellationToken);
                        break;
                    }
                }

                throw new WebSocketException(WebSocketError.Faulted, "Probe expected but received something else");
            } while (false);

            // Fetch server responses to 5
            try
            {
                r = await innerWebSocket.ReceiveAsync(bufferSegment, CancellationToken.None);
            }
            catch (WebSocketException ex)
            {
                throw new WebSocketException("Failed to receive Message+Connect response", ex);
            }

            do
            {
                if (r.MessageType == WebSocketMessageType.Text)
                {
                    var t = Encoding.UTF8.GetString(bufferSegment.Array, bufferSegment.Offset, r.Count);
                    if (t.StartsWith("40")) // Server should send "40" yet anything starts with it is ok. 
                        // engine.io<Message(4)> + socket.io<Connect(0)>
                        // OK!
                        break;
                    if (t.StartsWith("44") && t.Length >= 2)
                        // engine.io<Message(4)> + socket.io<Error(4)>
                        //
                        // If there is any application level error, 
                        // such as unauthorized access, invalid parameters
                        // socket.io will usually report the error only at this stage
                        throw new WebSocketException(WebSocketError.Faulted, "Fail to connect: " + t.Substring(2));
                    if (t.StartsWith("41"))
                        // engine.io<Message(4)> + socket.io<Disconnect(1)>
                        throw new WebSocketException(WebSocketError.Faulted,
                            "Server refuse to connect: " + t.Substring(2));
                    if (t.StartsWith("4"))
                        // engine.io<Message(4)> + anything else
                        //throw new WebSocketException(WebSocketError.Faulted, "Fail to connect: " + t);
                        break;
                }

                throw new WebSocketException(WebSocketError.Faulted,
                    "Message(4)+Connect(0) expected but received something else");
            } while (false);

            Connect?.Invoke(this, EventArgs.Empty);

            // Starting the loop
            new Thread(ReceiverLoop).Start(cancellationToken);
            new Thread(SenderLoop).Start(cancellationToken);
        }

        private Uri SanitiseUri(Uri uri)
        {
            return new UriBuilder
            {
                Port = uri.Port,
                Host = uri.Host,
                Path = uri.AbsolutePath,
                Query = (!string.IsNullOrEmpty(uri.Query) && uri.Query[0].Equals('?'))
                    ? uri.Query.Substring(1)
                    : uri.Query,
                Scheme = uri.Scheme
            }.Uri;
        }

        private string DebugUri(Uri uri)
        {
            var values = Enum.GetValues(typeof(UriComponents)).OfType<UriComponents>();
            return "{" + string.Join(", ",
                       values.Select(component => component.ToString() + "='" + uri.GetComponents(component, UriFormat.UriEscaped) + "'")) +
                   "}";
        }

        /// <summary>
        ///     Send a SocketIo EVENT
        /// </summary>
        /// <param name="payload">Json payload in the following format ["eventName", ..data..]</param>
        public void SendEventPayload(string payload, string nsp = "/")
        {
            SendRawData(Encoding.UTF8.GetBytes(
                "42" +
                (!string.IsNullOrWhiteSpace(nsp) && nsp != "/" ? nsp + "," : string.Empty) + payload
            ));
        }

        /// <summary>
        ///     Send a WebSocket packet
        /// </summary>
        /// <param name="data">packet. Could be null to wake-up the sender loop</param>
        public void SendRawData(byte[] data)
        {
            // Currently does not check for memory overflow
            senderQueue.Enqueue(data);
            lock (senderQueue)
            {
                Monitor.Pulse(senderQueue);
            }
        }

        private void WakeSenderLoop()
        {
            SendRawData(null);
        }

        /// <summary>
        ///     For SenderLoop to extract one item for sending
        /// </summary>
        /// <returns>A packet to be send</returns>
        private byte[] ExtractSenderQueue()
        {
            byte[] data;
            lock (senderQueue)
            {
                while (!senderQueue.TryDequeue(out data)) Monitor.Wait(senderQueue);
            }

            return data;
        }

        private async void SenderLoop(object obj)
        {
            var token = (CancellationToken) obj;

            byte[] data;
            while (innerWebSocket.State == WebSocketState.Open)
            {
                data = ExtractSenderQueue();
                if (innerWebSocket.State != WebSocketState.Open)
                    break;
                try
                {
                    if (Interlocked.Exchange(ref pingRequest, 0) == 1)
                    {
                        pingRequestAt = Environment.TickCount;
                        pongTimeoutDeadline = Environment.TickCount + (int) PingTimeout.TotalMilliseconds;
                        await innerWebSocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes("2")),
                            WebSocketMessageType.Text, true, CancellationToken.None);
                    }

                    if (data == null) continue;
                    await innerWebSocket.SendAsync(new ArraySegment<byte>(data),
                        WebSocketMessageType.Text, true, CancellationToken.None);

                    if (Interlocked.Exchange(ref pingRequest, 0) == 1)
                    {
                        pingRequestAt = Environment.TickCount;
                        pongTimeoutDeadline = Environment.TickCount + (int) PingTimeout.TotalMilliseconds;
                        await innerWebSocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes("2")),
                            WebSocketMessageType.Text, true, CancellationToken.None);
                    }
                }
                catch (WebSocketException e)
                {
                    // WebSocket is dead, quitting sliently. 
                    // Raise disconnect event in Receiver Loop
                    ModContent.GetInstance<TCCI>()?.Logger?.Warn("WebSocket dead", e);
                    break;
                }
                catch (ObjectDisposedException)
                {
                    // WebSocket is dead, quitting sliently. 
                    // Raise disconnect event in Receiver Loop
                    ModContent.GetInstance<TCCI>()?.Logger?.Warn("WebSocket disposed");
                    break;
                }

                if (!token.IsCancellationRequested) continue;
                try
                {
                    await innerWebSocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes("41")),
                        WebSocketMessageType.Text, true, CancellationToken.None);
                    await innerWebSocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes("1")),
                        WebSocketMessageType.Text, true, CancellationToken.None);
                }
                catch (Exception)
                {
                    // blah
                }
                finally
                {
                    await innerWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "",
                        CancellationToken.None);
                }

                break;
            }

            ModContent.GetInstance<TCCI>()?.Logger?.Warn("sender loop exited");
        }

        private async void ReceiverLoop(object obj)
        {
            var token = (CancellationToken) obj;
            WebSocketReceiveResult r;
            var buffer = new byte[BufferSize];
            var bufferSegment = new ArraySegment<byte>(buffer);

            pingRequestDeadline = Environment.TickCount /*+ (int)PingInterval.TotalMilliseconds*/;
            pongTimeoutDeadline = Environment.TickCount + (int) PingTimeout.TotalMilliseconds;
            Task<WebSocketReceiveResult> tReceive = null;
            Task tPing = null;
            while (innerWebSocket.State == WebSocketState.Open && !token.IsCancellationRequested)
            {
                try
                {
                    if (tReceive == null)
                        tReceive = innerWebSocket.ReceiveAsync(bufferSegment, CancellationToken.None);
                }
                catch (ObjectDisposedException)
                {
                    break;
                }

                if (tPing == null)
                    tPing = Task.Delay(Math.Max(0,
                        Math.Min(pingRequestDeadline - Environment.TickCount,
                            pongTimeoutDeadline - Environment.TickCount)));
                var t = await Task.WhenAny(tReceive, tPing);

                if (t == tReceive)
                {
                    try
                    {
                        r = await tReceive;
                        tReceive = null;
                    }
                    catch (WebSocketException e)
                    {
                        ModContent.GetInstance<TCCI>()?.Logger?.Error("Web socket exception!", e);
                        // Disconnection?
                        break;
                    }

                    switch (r.MessageType)
                    {
                        case WebSocketMessageType.Text:
                            if (r.Count > 0)
                                // Defalut engine.io protocol
                                switch (buffer[0])
                                {
                                    case (byte) '3': // Server Pong
                                        pongTimeoutDeadline =
                                            Environment.TickCount + (int) PingTimeout.TotalMilliseconds +
                                            (int) PingInterval.TotalMilliseconds;
                                        Ping?.Invoke(this, new PingEventArgs(pingRequestAt, Environment.TickCount));
                                        break;

                                    case (byte) '4': // Message
                                        if (r.Count > 1)
                                            // Defalut socket.io protocol
                                            switch (buffer[1])
                                            {
                                                case (byte) '0': // Connect
                                                case (byte) '1': // Disconnect
                                                    // Ignored
                                                    break;
                                                case (byte) '2': // Event
                                                    try
                                                    {
                                                        var text = Encoding.UTF8.GetString(bufferSegment.Array,
                                                            bufferSegment.Offset + 2, r.Count - 2);
                                                        ModContent.GetInstance<TCCI>()?.Logger?.Debug("Incoming WebSocket event: " + text);
                                                        IncomingEvent?.Invoke(this, new IncomingEventEventArgs(text));
                                                    }
                                                    catch (ArgumentException)
                                                    {
                                                    }

                                                    break;
                                                case (byte) '3': // Ack
                                                case (byte) '4': // Error
                                                case (byte) '5': // Binary_Event
                                                case (byte) '6': // Binary_Ack
                                                    // Ignored
                                                    break;
                                            }

                                        break;
                                }

                            break;
                        case WebSocketMessageType.Binary:
                        case WebSocketMessageType.Close:
                        default:
                            // Nothing to handle
                            break;
                    }
                }
                else
                {
                    await tPing;
                    tPing = null;

                    if (Environment.TickCount - pingRequestDeadline >= 0)
                        if (Interlocked.CompareExchange(ref pingRequest, 1, 0) == 0)
                        {
                            pingRequest = 1;
                            WakeSenderLoop();
                            pingRequestDeadline = Environment.TickCount + (int) PingInterval.TotalMilliseconds;
                            pongTimeoutDeadline = Environment.TickCount + (int) PingTimeout.TotalMilliseconds;
                        }

                    if (Environment.TickCount - pongTimeoutDeadline >= 0)
                    {
                        ModContent.GetInstance<TCCI>()?.Logger?.Warn("Ping timeout at " + Environment.TickCount + ", current deadline: " +
                                                                   pongTimeoutDeadline);
                        // Ping timeout
                        try
                        {
                            await innerWebSocket.CloseAsync(WebSocketCloseStatus.EndpointUnavailable, "Ping timeout",
                                CancellationToken.None);
                        }
                        catch (WebSocketException)
                        {
                        }
                        catch (ObjectDisposedException)
                        {
                        }

                        break;
                    }
                }
            }

            Disconnect?.Invoke(this, EventArgs.Empty);
        }
    }
}