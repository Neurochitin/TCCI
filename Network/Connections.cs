using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IrcDotNet;
using Newtonsoft.Json.Linq;
using TCCI.Data;
using Terraria.ModLoader;

namespace TCCI.Network
{
    // Should be called "EventEventArgs" but that's stupid...
    public class StreamEventArgs : EventArgs
    {
        public EventInfo Info;
        public EventType Type;
    }

    [Flags]
    public enum ConnectionType
    {
        None = 0,
        Irc = 1,
        StreamLabs = 2
    }

    internal static class ConnectionTypeMethods
    {
        public static string ToStringIndividual(this ConnectionType type)
        {
            return type == ConnectionType.Irc ? "chat" : "StreamLabs";
        }
    }

    public class ConnectionEventArgs : EventArgs
    {
        public ConnectionType Type;
    }

    public class ConnectionErrorEventArgs : ConnectionEventArgs
    {
        public string Message;
    }

    public class Connections
    {
        private TwitchIrcClient _ircClient;
        private SocketIoClient _socketClient;

        private CancellationTokenSource _socketClientToken;

        private ConnectionType _activeConnections = ConnectionType.None;
        public ConnectionType ActiveConnections => _activeConnections;

        private Uri _uri;

        public event EventHandler<StreamEventArgs> Event;
        public event EventHandler<ConnectionEventArgs> Connected;
        public event EventHandler<ConnectionEventArgs> Disconnected;
        public event EventHandler<ConnectionErrorEventArgs> ConnectionError;

        private void InitialiseIrc()
        {
            _ircClient = new TwitchIrcClient();
            _ircClient.FloodPreventer = new IrcStandardFloodPreventer(4, 2000);
            _ircClient.Disconnected += (sender, args) =>
            {
                _activeConnections &= ~ConnectionType.Irc;
                Disconnected?.Invoke(this,
                    new ConnectionEventArgs {Type = ConnectionType.Irc});
            };
            _ircClient.Registered += IrcClientOnRegistered;
        }

        private void IrcClientOnRegistered(object sender, EventArgs e)
        {
            var client = (IrcClient) sender;
            client.LocalUser.JoinedChannel += (sender2, args2) =>
            {
                args2.Channel.MessageReceived += IrcChannelOnMessageReceived;
            };

            client.Channels.Join("#" + ModContent.GetInstance<TCCIConfig>().TwitchChannel);
        }

        private void IrcChannelOnMessageReceived(object sender, IrcMessageEventArgs e)
        {
            var prefix = ModContent.GetInstance<TCCIConfig>().CommandPrefix;
            if (e.Text.StartsWith(prefix))
            {
                var command = e.Text.Split()[0].Substring(prefix.Length);
                var args = new StreamEventArgs
                {
                    Info = new ChatCommandEventInfo
                    {
                        Command = command.ToLower(), IsTest = false,
                        Name = null, Priority = 0
                    },
                    Type = EventType.ChatCommand
                };
                Event?.Invoke(this, args);
            }
        }

        private void InitialiseStreamlabs()
        {
            var builder = new UriBuilder("https://sockets.streamlabs.com");
            builder.Query = "token=" + ModContent.GetInstance<TCCIConfig>().StreamlabsToken;
            _uri = builder.Uri;

            _socketClient = new SocketIoClient();
            _socketClient.IncomingEvent += SocketClientOnIncomingEvent;
            _socketClient.Connect += (sender, args) =>
            {
                _activeConnections |= ConnectionType.StreamLabs;
                Connected?.Invoke(this, new ConnectionEventArgs {Type = ConnectionType.StreamLabs});
            };
            _socketClient.Disconnect += (sender, args) =>
            {
                _activeConnections &= ~ConnectionType.StreamLabs;
                Disconnected?.Invoke(this, new ConnectionEventArgs {Type = ConnectionType.StreamLabs});
            };
        }

        private void SocketClientOnIncomingEvent(object sender, IncomingEventEventArgs e)
        {
            try
            {
                var payload = JArray.Parse(e.Payload);
                if ((string) payload[0] == "event")
                {
                    var data = (JObject) payload[1];
                    var type = (string) data["type"];
                    var messages = (JArray) data["message"];

                    foreach (var jToken in messages)
                    {
                        var message = (JObject) jToken;
                        var eventType = EventTypeUtil.Parse(type);
                        EventInfo eventInfo = null;
                        Type eventInfoType = EventTypeUtil.GetEventInfoType(eventType);

                        if (eventInfoType != null)
                        {
                            var toObjMethod = typeof(JObject)
                                .GetMethods()
                                .Where(x => x.Name == "ToObject")
                                .FirstOrDefault(x => x.IsGenericMethod);
                            toObjMethod = toObjMethod?.MakeGenericMethod(eventInfoType);
                            eventInfo = (EventInfo) toObjMethod?.Invoke(message, null);
                        }
                        else ModContent.GetInstance<TCCI>().Logger.Error("Got payload that is an unknown event type: " + e.Payload);

                        if (eventInfo == null)
                            ModContent.GetInstance<TCCI>().Logger
                                .Warn("EventInfo is null, next command invocation might fail!");
                        
                        var args = new StreamEventArgs
                        {
                            Info = eventInfo,
                            Type = eventType
                        };

                        Event?.Invoke(this, args);
                    }
                }
                else
                {
                    ModContent.GetInstance<TCCI>().Logger.Warn("Got payload that is not an event: " + e.Payload);
                }
            }
            catch (Exception ex)
            {
                ModContent.GetInstance<TCCI>().Logger.Error("Error while parsing event!" + ex);
            }
        }

        public void Connect(ConnectionType type)
        {
            if (type.HasFlag(ConnectionType.StreamLabs)) ConnectStreamlabs();
            if (type.HasFlag(ConnectionType.Irc)) ConnectIrc();
        }

        public void Disconnect(ConnectionType type)
        {
            if (type.HasFlag(ConnectionType.StreamLabs)) DisconnectStreamlabs();
            if (type.HasFlag(ConnectionType.Irc)) DisconnectIrc();
        }

        private void DisconnectStreamlabs()
        {
            _socketClientToken?.Cancel();
            try
            {
                _socketClient?.Dispose();
            }
            catch (ObjectDisposedException)
            {
            }
        }

        private void DisconnectIrc()
        {
            try
            {
                _ircClient?.Disconnect();
                _ircClient?.Dispose();
            }
            catch (ObjectDisposedException)
            {
            }
        }

        private void ConnectIrc()
        {
            InitialiseIrc();

            var username = "justinfan" + new Random().Next(1000, 9999);
            var password = "";

            var connectionTimeout = ModContent.GetInstance<TCCIConfig>().IrcConnectionTimeout;

            using (var registeredEvent = new ManualResetEventSlim(false))
            {
                using (var connectedEvent = new ManualResetEventSlim(false))
                {
                    _ircClient.Connected += (sender2, e2) => connectedEvent.Set();
                    _ircClient.Registered += (sender2, e2) => registeredEvent.Set();
                    _ircClient.Connect("irc.chat.twitch.tv", false,
                        new IrcUserRegistrationInfo
                        {
                            NickName = username,
                            Password = password,
                            UserName = username
                        });
                    if (!connectedEvent.Wait(connectionTimeout))
                    {
                        ConnectionError?.Invoke(this,
                            new ConnectionErrorEventArgs
                                {Message = "IRC connection timed out", Type = ConnectionType.Irc});
                        return;
                    }
                }

                if (!registeredEvent.Wait(connectionTimeout))
                    ConnectionError?.Invoke(this,
                        new ConnectionErrorEventArgs
                            {Message = "IRC registration timed out", Type = ConnectionType.Irc});

                _activeConnections |= ConnectionType.Irc;
                Connected?.Invoke(this, new ConnectionEventArgs {Type = ConnectionType.Irc});
            }
        }

        private void ConnectStreamlabs()
        {
            InitialiseStreamlabs();

            try
            {
                _socketClientToken = new CancellationTokenSource();
                Task.Run(async () => await _socketClient.ConnectAsync(_uri, _socketClientToken.Token));
            }
            catch (Exception e)
            {
                ConnectionError?.Invoke(this,
                    new ConnectionErrorEventArgs
                    {
                        Message = "Exception occurred while connecting to Streamlabs: " + e,
                        Type = ConnectionType.StreamLabs
                    });
            }
        }
    }
}