using System.Collections.Generic;
using Terraria.ModLoader;

namespace TCCI.Scripting
{
    // I do not trust my users quite enough to understand the implications of arbitrary code execution.
    // Therefore this class implements some very basic filters to prevent the worst of the worst.
    // But there are absolutely still workarounds! If you find one, please tell me about it and I will try to add it to the checks.
    public class AntiVirus
    {
        private readonly string _code, _codelc;

        public AntiVirus(string scriptCode)
        {
            _code = scriptCode;
            _codelc = scriptCode.ToLower();
        }

        public List<string> MatchedKeywords { get; } = new List<string>();

        public bool IsScriptBad => _IsScriptBad();

        private bool _IsScriptBad()
        {
            if (!ModContent.GetInstance<TCCIConfig>().AntiVirusEnabled) return false;

            // Reflection
            if (
                Matches("GetType") || Matches("GetMethod") || Matches("GetField") ||
                Matches("GetProperty") // standard reflection stuff
                || Matches("typeof")
                || Matches("Reflection") // to prevent accessing stuff in System.Reflection directly
                || MatchesLc("assembly") || MatchesLc("assemblies") // messing around with assemblies
                || Matches("Marshal") // If you are messing around with marshal things, you're probably up to no good!
            ) return true;

            // Win32 APIs (like registry stuff), linking to native code (not sure whether this is possible anyway)
            if (MatchesLc("win32") || MatchesLc("win16") || MatchesLc("win64") || Matches("DllImport")) return true;

            // File system
            if (Matches("IO") || Matches("File") || Matches("Directory")) return true;

            return false;
        }

        private bool Matches(string keyword)
        {
            return MatchesGeneric(_code, keyword);
        }

        private bool MatchesLc(string keyword)
        {
            return MatchesGeneric(_codelc, keyword);
        }

        private bool MatchesGeneric(string code, string keyword)
        {
            if (code.Contains(keyword))
            {
                MatchedKeywords.Add(keyword);
                return true;
            }

            return false;
        }
    }
}