using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.GameContent.Events;
using Terraria.ID;
using Terraria.Localization;
using Terraria.Utilities;

namespace TCCI.Scripting
{
    // Implements some utility methods to be used in scripts. The Script class (superclass of all script classes)
    // extends this class, so all methods can be use directly without referring to this class.
    public abstract class UtilityMethods
    {
        // The current player. In multiplayer, this will refer to an arbitrary player.
        public static Player Player => Main.netMode == NetmodeID.SinglePlayer
            ? Main.player[Main.myPlayer]
            : TargetedPlayer ?? Players.First() ?? Main.player[0];
        
        // The targeted player. If this is set in multiplayer, the above `Player` property will refer to this. Use
        // /tcci targetme to set yourself as the target, or you can set this field directly in scripts
        public static Player TargetedPlayer = null;
        
        // The list of players currently active.
        public static Player[] Players => Main.player.Where(player => player.active).ToArray();
        
        // An instance of UnifiedRandom
        public static UnifiedRandom Random { get; } = new UnifiedRandom();

        // Tick actions
        public static List<TickAction> TickActions { get; } = new List<TickAction>();

        // Tile offsets around the player
        public static int TileAbovePlayer => -1;
        public static int TileLeftOfPlayer => -1;
        public static int TileBelowPlayer => (Player.position.Y % 16 > 8) ? 4 : 3;
        public static int TileRightOfPlayer => (Player.position.X % 16 > 8) ? 3 : 2;

        public static Item[] Inventory => Player.inventory.Concat(Player.armor).Concat(Player.dye)
            .Concat(Player.miscEquips).Concat(Player.miscDyes).ToArray();

        // Applies a buff to the specified player, or the current Player if none is specified.
        public static void AddBuff(int buffId, int duration, Player player = null)
        {
            if (player == null) player = Player;
            if (Main.netMode == NetmodeID.Server)
            {
                NetMessage.SendData(MessageID.AddPlayerBuff, number: player.whoAmI, number2: buffId, number3: duration);
            }
            else
            {
                player.AddBuff(buffId, duration);
            }
        }
        
        // Writes a message to chat. In multiplayer, the message is broadcasted to all players.
        public static void ChatMessage(string text, byte red = 255, byte green = 255, byte blue = 255)
        {
            if (Main.netMode == NetmodeID.Server)
            {
                NetMessage.BroadcastChatMessage(NetworkText.FromLiteral(text), new Color(red, green, blue));
            }
            else
            {
                Main.NewText(text, red, green, blue);
            }
        }

        // Executes an action for all NPCs (includes enemies and critters; not just town NPCs) in a certain radius
        // around the player.
        public static void ForAllNPCsWithin(double blocks, Action<NPC> todo)
        {
            foreach (var npc in Main.npc)
                if ((Player.position - npc.position).Length() <= blocks * 16)
                    todo.Invoke(npc);
        }

        // Spawns a new NPC (enemy, etc.) at the player, with the given offsets towards the right and bottom. Negative
        // values for those offsets are of course possible.
        public static NPC SpawnNPCAtPlayer(int npc, double offsetTowardsRightBlocks = 0.0,
            double offsetTowardsBottomBlocks = 0.0)
        {
            int idx = NPC.NewNPC((int) (Player.position.X + offsetTowardsRightBlocks * 16),
                (int) (Player.position.Y + offsetTowardsBottomBlocks * 16), npc);
            return Main.npc[idx];
        }

        // Summons a projectile at the player's location.
        public static void QuickProjectile(int projectile, float speedX, float speedY, int damage = 0, float knockback = 0)
        {
            Projectile.NewProjectile(Player.position, new Vector2(speedX, speedY), projectile, damage, knockback,
                Main.myPlayer);
        }

        // Summons a projectile at an offset from the player.
        public static void QuickProjectileAtOffset(int projectile, float offsetX, float offsetY, float speedX, float speedY, int damage = 0, float knockback = 0)
        {
            float x = Player.position.X + offsetX * 16;
            float y = Player.position.Y + offsetY * 16;
            Projectile.NewProjectile(new Vector2(x, y), new Vector2(speedX, speedY), projectile, damage, knockback,
                Main.myPlayer);
        }

        // Randomly selects one of a bunch of options.
        public static T RandomlySelect<T>(params T[] values)
        {
            return values[Random.Next(values.Length)];
        }

        // Place a wall at an offset from the player.
        public static void PlaceWallAtOffset(int wall, int tileOffsetX, int tileOffsetY)
        {
            int x = ((int) Player.position.X / 16) + tileOffsetX;
            int y = ((int) Player.position.Y / 16) + tileOffsetY;
            WorldGen.PlaceWall(x, y, wall);
        }

        // Place a tile (or other placeable object) at an offset from the player.
        public static void PlaceTileAtOffset(int tile, int tileOffsetX, int tileOffsetY, int style = 0, bool forced = false)
        {
            int x = ((int) Player.position.X / 16) + tileOffsetX;
            int y = ((int) Player.position.Y / 16) + tileOffsetY;
            Vector2 position = Player.position + Vector2.One.RotateRandom(6.28 /* 2 * pi */) * (16 * 100);
            
            WorldGen.PlaceTile(x, y, tile, forced: forced, style: style);
        }

        // Get the tile that exists at a specific offset from the player.
        public static Tile GetTileAtOffset(int tileOffsetX, int tileOffsetY)
        {
            int x = ((int) Player.position.X / 16) + tileOffsetX;
            int y = ((int) Player.position.Y / 16) + tileOffsetY;
            return Main.tile[x, y];
        }

        // Destroy the tile located at a specific offset from the player.
        public static void KillTileAtOffset(int tileOffsetX, int tileOffsetY, bool noItem = false)
        {
            int x = ((int) Player.position.X / 16) + tileOffsetX;
            int y = ((int) Player.position.Y / 16) + tileOffsetY;
            WorldGen.KillTile(x, y, noItem: noItem);
        }

        // Place a liquid at an offset from the player. liquidType is one of Tile.Liquid_Water, Tile.Liquid_Lava, Tile.Liquid_Honey
        public static void PlaceLiquidAtOffset(int liquidType, int tileOffsetX, int tileOffsetY)
        {
            int x = ((int) Player.position.X / 16) + tileOffsetX;
            int y = ((int) Player.position.Y / 16) + tileOffsetY;
            
            Main.PlaySound(19, (int) Player.position.X, (int) Player.position.Y);
            Main.tile[x, y].liquidType(liquidType);
            Main.tile[x, y].liquid = byte.MaxValue;
            WorldGen.SquareTileFrame(x, y);
            if (Main.netMode == 1)
                NetMessage.sendWater(x, y);
        }

        // Generates a random float in a range
        public static float RandomFloat(float min, float max)
        {
            return (float) Random.NextDouble() * (max - min) + min;
        }

        public static float RandomFloat(double min, double max)
        {
            return (float) Random.NextDouble() * ((float) max - (float) min) + (float) min;
        }

        public static float RandomFloat(float min, double max)
        {
            return (float) Random.NextDouble() * ((float) max - min) + min;
        }

        public static float RandomFloat(double min, float max)
        {
            return (float) Random.NextDouble() * (max - (float) min) + (float) min;
        }

        // Generates a random integer in a range
        public static int RandomInteger(int minInclusive, int maxInclusive)
        {
            return Random.Next(minInclusive, maxInclusive + 1);
        }

        // Sets an action to be executed for the next countTicks ticks. The action will be called with the number of
        // remaining ticks as the parameter
        public static void NextTicks(int countTicks, Action<int> action)
        {
            TickAction ta = new TickAction
            {
                RemainingTickCount = countTicks,
                Action = action
            };
            
            TickActions.Add(ta);
        }

        // Sleeps for a specific amount of milliseconds
        public static void Sleep(int ms)
        {
            Thread.Sleep(ms);
        }
        
        // Executes tick actions (not to be used by scripts)
        internal static void ExecuteTickActions()
        {
            TickActions.RemoveAll(ta => ta.RemainingTickCount <= 0);
                
            foreach (TickAction ta in TickActions)
            {
                ta.Action(ta.RemainingTickCount);
                ta.RemainingTickCount--;
            }
        }

    }

    public class TickAction
    {
        public int RemainingTickCount;
        public Action<int> Action;
    }
}