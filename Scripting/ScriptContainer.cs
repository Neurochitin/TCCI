using System.Reflection;
using TCCI.Data;

namespace TCCI.Scripting
{
    public class ScriptContainer
    {
        public Assembly Assembly;
        public ScriptCategory Category;
        public string Name;
    }

    public class ScriptCategory
    {
        public readonly EventType Event;
        public readonly string Parameter;
        public readonly ScriptType Type;

        private ScriptCategory(ScriptType type, EventType @event, string parameter)
        {
            Type = type;
            Event = @event;
            Parameter = parameter;
        }

        protected bool Equals(ScriptCategory other)
        {
            return Type == other.Type && Event == other.Event && Parameter == other.Parameter;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ScriptCategory) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int) Type;
                if (Type == ScriptType.Always) return hashCode;
                hashCode = (hashCode * 397) ^ (int) Event;
                if (Type != ScriptType.Specific) return hashCode;
                hashCode = (hashCode * 397) ^ (Parameter != null ? Parameter.GetHashCode() : 0);
                return hashCode;
            }
        }

        public bool Matches(EventType eventType, string parameter)
        {
            switch (Type)
            {
                case ScriptType.Always:
                    return true;
                case ScriptType.Generic:
                    return Event == eventType;
                case ScriptType.Specific:
                    return Event == eventType && Parameter == parameter;
                default:
                    return false;
            }
        }

        public static ScriptCategory Parse(string scriptName)
        {
            if (scriptName == "always") return new ScriptCategory(ScriptType.Always, EventType.None, "");
            if (scriptName == ScriptLoader.TemplateScriptName) return null;

            if (scriptName.StartsWith("command"))
            {
                if (scriptName == "command") return new ScriptCategory(ScriptType.Generic, EventType.ChatCommand, "");

                var commandName = scriptName.Substring(7).ToLower();
                return new ScriptCategory(ScriptType.Specific, EventType.ChatCommand, commandName);
            }

            var digitIndex = scriptName.IndexOfAny("0123456789".ToCharArray());
            var initialPart = scriptName;
            var parameter = "";
            var type = ScriptType.Generic;

            // Check whether there is a digit at any point in the script name. If there is, try to parse it (the script name should be like "bits400")
            if (digitIndex >= 0)
            {
                initialPart = scriptName.Substring(0, digitIndex);
                var finalPart = scriptName.Substring(digitIndex);
                int _;
                if (!int.TryParse(finalPart, out _)) return null;
                parameter = finalPart;
                type = ScriptType.Specific;
            }

            var evt = EventTypeUtil.Parse(initialPart);
            if (evt == EventType.None) return null;

            return new ScriptCategory(type, evt, parameter);
        }
    }

    public enum ScriptType
    {
        Always = 0,
        Generic = 1,
        Specific = 2
    }
}