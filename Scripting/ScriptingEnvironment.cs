using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TCCI.Data;
using Terraria.ModLoader;

namespace TCCI.Scripting
{
    // This class tracks changes in script and mod code over time, so it can be determined whether some or all scripts need to be recompiled.
    public class ScriptingEnvironment
    {
        private static readonly string EnvironmentFilePath =
            Path.Combine(ScriptLoader.TCCIReferencesFolderPath, "environment.json");

        private readonly MD5 _hash;
        private Dictionary<string, string> _data = new Dictionary<string, string>();

        public ScriptingEnvironment()
        {
            _hash = MD5.Create();
        }

        public bool IsClean { get; private set; }

        public void MarkDirty()
        {
            IsClean = false;
        }

        public void Load()
        {
            _data = JsonUtil.LoadJsonDictionaryFromFile<string, string>(EnvironmentFilePath);
            IsClean = true;
            UpdateLoadedMods();
        }

        public void Save()
        {
            JsonUtil.WriteJsonDictionaryToFile(EnvironmentFilePath, _data);
        }

        public bool IsScriptClean(string scriptName, string scriptCode)
        {
            if (!IsClean) return false;
            return _data.ContainsKey(scriptName) && _data[scriptName] == Hash(scriptCode);
        }

        private string Hash(string data)
        {
            return Hash(Encoding.UTF8.GetBytes(data));
        }

        private string Hash(byte[] data)
        {
            var hashBytes = _hash.ComputeHash(data);
            var sb = new StringBuilder();
            foreach (var b in hashBytes) sb.Append(b.ToString("X2"));
            return sb.ToString();
        }

        public void UpdateScript(string scriptName, string scriptCode)
        {
            _data[scriptName] = Hash(scriptCode);
        }

        public void UpdateModAssemblyData(Mod mod, byte[] assemblyData)
        {
            var key = "_mod:" + mod.Name;
            var modHash = Hash(assemblyData);
            if (_data.ContainsKey(key) && modHash == _data[key]) return; // Nothing changed

            // This mod assembly changed since the last time it was loaded! So we are in a dirty environment.
            IsClean = false;
            _data[key] = modHash;
        }

        private void UpdateLoadedMods()
        {
            var currentModsHash =
                Hash(string.Join(";", ModLoader.Mods.Select(mod => mod.Name).OrderBy(name => name)));
            if (_data.ContainsKey("_mods") && currentModsHash == _data["_mods"]) return;
            IsClean = false;
            _data["_mods"] = currentModsHash;
        }
    }
}