using System;
using System.Collections.Generic;
using TCCI.Data;

namespace TCCI.Scripting
{
    public class CooldownTracker
    {
        private readonly Dictionary<string, long> _lastExecuteTime = new Dictionary<string, long>();
        private readonly CooldownValueStore _valueStore;

        public bool CooldownsEnabled = true;

        public CooldownTracker(CooldownValueStore valueStore)
        {
            _valueStore = valueStore;
        }

        public bool IsScriptOnCooldown(string scriptName)
        {
            if (!CooldownsEnabled) return false;
            if (!_lastExecuteTime.ContainsKey(scriptName)) return false;
            return Environment.TickCount <= GetTimeOfNextExecution(scriptName);
        }

        private long GetTimeOfNextExecution(string scriptName)
        {
            return _lastExecuteTime[scriptName] + 1000 * _valueStore[scriptName];
        }

        public long GetRemainingCooldownTime(string scriptName)
        {
            return GetTimeOfNextExecution(scriptName) - Environment.TickCount;
        }

        public void NotifyScriptExecution(string scriptName)
        {
            _lastExecuteTime[scriptName] = Environment.TickCount;
        }

        public void ResetCooldown(string scriptName)
        {
            _lastExecuteTime.Remove(scriptName);
        }

        public void ResetAllCooldowns()
        {
            _lastExecuteTime.Clear();
        }
    }
}