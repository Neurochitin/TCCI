using System;
using TCCI.Data;
using Terraria.ModLoader;

namespace TCCI.Scripting
{
    public class ScriptExecutor
    {
        
        public ScriptExecutor()
        {
            CooldownTracker = new CooldownTracker(CooldownValueStore);
        }

        public CooldownValueStore CooldownValueStore { get; } = new CooldownValueStore();
        public CooldownTracker CooldownTracker { get; }
        public ScriptContainer LastExecutedScript { get; private set; }
        public EventInfo LastEventInfo { get; private set; }

        public void Execute(ScriptContainer script, EventInfo eventInfo)
        {
            try
            {
                if (CooldownTracker.IsScriptOnCooldown(script.Name))
                {
                    ModContent.GetInstance<TCCI>().Logger
                        .Debug("Not executing (because of cooldown) script " + script.Name);
                    return;
                }

                CooldownTracker.NotifyScriptExecution(script.Name);
                LastExecutedScript = script;
                LastEventInfo = eventInfo;
                var ctx = new ExecutionContext(eventInfo, script);

                var mainClassName = "TCCIScripts.Script_" + script.Name;
                var scriptInstance = script.Assembly.CreateInstance(mainClassName);
                var type = scriptInstance.GetType();
                var method = type.GetMethod("Run");
                method.Invoke(scriptInstance, new object[] {ctx});
            }
            catch (Exception e)
            {
                TCCI.Message("Tried to execute script " + script.Name + ", but execution failed due to an exception! (See the log file for details)");
                ModContent.GetInstance<TCCI>().Logger.Error("The exception that caused execution to fail: ", e);
            }
        }
    }
}