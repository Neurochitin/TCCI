using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using log4net;
using Microsoft.Xna.Framework;
using TCCI.Data;
using TCCI.UI;
using Terraria;
using Terraria.ModLoader;

namespace TCCI.Scripting
{
    public class ScriptLoader
    {
        public const string TemplateScriptName = "template";
        private const string ScriptNamePlaceholder = "SCRIPTNAME";
        private const string ScriptCodePlaceholder = "/* CODE */";
        public static string ScriptAssemblyFolderPath = Path.Combine(TCCIReferencesFolderPath, "scripts");

        private readonly Dictionary<string, string> _compilationOutput = new Dictionary<string, string>();
        private readonly HashSet<string> _erroredScripts = new HashSet<string>();

        public readonly ScriptingEnvironment Environment = new ScriptingEnvironment();

        public readonly Dictionary<ScriptCategory, ScriptContainer> LoadedScripts =
            new Dictionary<ScriptCategory, ScriptContainer>();

        private CodeDomProvider _cdp;
        private CompilerParameters _params;

        private string _template;
        public static string ScriptCodeFolderPath => Path.Combine(Main.SavePath, "TCCI Scripts");
        public static string TCCIReferencesFolderPath => Path.Combine(Main.SavePath, "references.tcci");

        private Assembly[] AssembliesToLoad => new[]
        {
            typeof(Main).Assembly, // Terraria itself
            typeof(Mod).Assembly, // tModLoader (this is usually the same assembly as Terraria itself; just to make sure)
            typeof(Color).Assembly, // XNA or FNA
            typeof(Game).Assembly, // also XNA (Game.dll)
            typeof(ILog).Assembly // log4net (for good measure)
            // Explicitly loading mscorlib is not necessary because it will be present anyway.
        };

        public void ReloadScripts(CommandCaller caller, bool forceCleanReload = false)
        {
            // Preparations...
            EnsureFoldersExist();
            Environment.Load();
            EnsureTemplateScriptExists();
            _template = File.ReadAllText(GetScriptCodePath(TemplateScriptName));
            PrepareCompilation();
            LoadedScripts.Clear();
            if (forceCleanReload) Environment.MarkDirty();
            if (!Environment.IsClean) ClearScriptAssemblyFolder();

            // Look for scripts that exist
            var scriptFiles = GetValidScriptFiles(caller);
            ReloadProgressUI.Reset(scriptFiles.Length);

            // Attempt to load cached scripts
            var toCompile = new List<ScriptDefinition>();
            foreach (var script in scriptFiles)
            {
                ReloadProgressUI.CurrentScriptName = script.Name;
                script.Code = ReadScript(script.Name);
                if (!ScriptPassesAntiVirus(caller, script)) continue;
                var assembly = TryLoadCachedAssembly(script);
                if (assembly != null)
                {
                    ConstructAndLoadContainer(script, assembly);
                    ReloadProgressUI.CurrentScript++;
                }
                else toCompile.Add(script);
            }

            // Notify user (the next step might take a bit)
            int numToCompile = toCompile.Count, numCached = scriptFiles.Length - numToCompile;
            caller.Reply("Found " + scriptFiles.Length + " scripts, successfully loaded " + numCached +
                         " of these from cache. Compiling " + numToCompile + " scripts from scratch." +
                         (numToCompile > 5 ? " (This might take a while!)" : ""));

            // Compile remaining scripts
            foreach (var script in toCompile)
            {
                ReloadProgressUI.CurrentScriptName = script.Name;
                var assembly = CompileScript(caller, script);
                ConstructAndLoadContainer(script, assembly);
                ReloadProgressUI.CurrentScript++;
            }

            ReloadProgressUI.Visible = false;
            Environment.Save();
        }

        private bool ScriptPassesAntiVirus(CommandCaller caller, ScriptDefinition script)
        {
            var av = new AntiVirus(script.Code);
            if (!av.IsScriptBad) return true;

            var keywordStr = string.Join(", ", av.MatchedKeywords.Select(str => $"'{str}'"));
            caller.Reply(
                $"Not loading script '{script.Name}' because the AntiVirus was triggered! Offending keywords: {keywordStr}");
            return false;
        }

        private void ClearScriptAssemblyFolder()
        {
            foreach (var file in new DirectoryInfo(ScriptAssemblyFolderPath).EnumerateFiles()) file.Delete();
        }

        private void ConstructAndLoadContainer(ScriptDefinition script, Assembly assembly)
        {
            var container = new ScriptContainer
            {
                Assembly = assembly,
                Category = script.Category,
                Name = script.Name
            };
            LoadedScripts[script.Category] = container;
        }

        private ScriptDefinition[] GetValidScriptFiles(CommandCaller caller)
        {
            return ScanScriptFolder().Select(Path.GetFileNameWithoutExtension)
                .Where(scriptName => scriptName != TemplateScriptName)
                .Select(scriptName =>
                {
                    var category = ScriptCategory.Parse(scriptName);
                    if (category == null) caller.Reply("Skipping script '" + scriptName + "' with invalid name");
                    return new ScriptDefinition
                    {
                        Category = category,
                        Name = scriptName
                    };
                })
                .Where(def => def.Category != null)
                .ToArray();
        }

        private Assembly TryLoadCachedAssembly(ScriptDefinition script)
        {
            if (Environment.IsScriptClean(script.Name, script.Code))
            {
                // The script code has not been changed since last time. Try to load a cached assembly.
                var cachedAssembly = LoadCachedAssembly(script.Name);
                if (cachedAssembly != null)
                {
                    ModContent.GetInstance<TCCI>().Logger.Debug("Using cached assembly for script " + script.Name);
                    return cachedAssembly;
                }
            }

            return null;
        }

        private Assembly LoadCachedAssembly(string scriptName)
        {
            var path = GetScriptAssemblyPath(scriptName);
            if (!File.Exists(path)) return null;

            try
            {
                var data = File.ReadAllBytes(path);
                var cachedAssembly = Assembly.Load(data);
                return cachedAssembly;
            }
            catch (Exception e)
            {
                ModContent.GetInstance<TCCI>().Logger.Error(
                    "Not loading cached assembly at " + path + " because of exception while loading!", e);
                return null;
            }
        }

        public string GetCompilationOutput(string scriptName)
        {
            return _compilationOutput[scriptName];
        }

        public IEnumerable<ScriptContainer> GetLoadedScriptsFor(EventType type, string parameter)
        {
            return LoadedScripts.Keys.Where(category => category.Matches(type, parameter))
                .OrderBy(category => (int) category.Type)
                .Select(category => LoadedScripts[category]);
        }

        private void PrepareCompilation()
        {
            _cdp = CodeDomProvider.CreateProvider("CSharp");
            _params = new CompilerParameters();

            var assemblies = GetAssemblyPaths();
            _params.ReferencedAssemblies.AddRange(assemblies);
            ModContent.GetInstance<TCCI>().Logger.Debug("Referenced assemblies: " + string.Join("; ", assemblies));

            _params.GenerateExecutable = false;
            _params.GenerateInMemory = false;

            _compilationOutput.Clear();
        }

        private string[] GetAssemblyPaths()
        {
            // C#'s "runtime" compilation is really just a wrapper around shell commands. So it doesn't allow runtime-compiled code to
            // refer to in-memory assemblies.
            // However, tModLoader loads its mods as in-memory assemblies. We want scripts to be able to access classes from other mods
            // (if only TCCI itself, so it can access the ExecutionContext, but also so scripts can do fun things with content from other mods).
            // So we must write the in-memory assemblies to temporary files.
            var modAssemblyPaths = WriteModAssemblies();

            // Get assemblies to load. This is deliberately very restricted (e.g. not even System.dll is loaded) to avoid security problems.
            // If your code needs some external assembly that isn't loaded by default, just add it to ExtraAssemblyPaths
            var assemblies = AssembliesToLoad
                .Select(assembly => assembly.Location)
                .Concat(modAssemblyPaths)
                .Concat(ModContent.GetInstance<TCCIConfig>().ExtraAssemblyPaths)
                .Distinct();

            return assemblies.ToArray();
        }

        private List<string> WriteModAssemblies()
        {
            var di = new DirectoryInfo(TCCIReferencesFolderPath);

            // Remove all existing files
            foreach (var file in di.EnumerateFiles()) file.Delete();

            // Write mod assemblies to files
            var result = new List<string>();
            foreach (var mod in ModLoader.Mods)
            {
                ModContent.GetInstance<TCCI>().Logger.Debug("Mod " + mod.Name + ": Assembly " + mod.Code + " at " + mod.Code?.Location);
                if (mod.Code == null) continue;

                var assemblyData = GetModAssemblyData(mod);
                Environment.UpdateModAssemblyData(mod, assemblyData);
                var newPath = Path.Combine(TCCIReferencesFolderPath, mod.Name + ".dll");
                File.WriteAllBytes(newPath, assemblyData);
                result.Add(newPath);
            }

            return result;
        }

        private static byte[] GetModAssemblyData(Mod mod)
        {
            // Use reflection to access the dictionary where tML stores binary data of existing assemblies.
            // Fortunately this information is even stored anywhere! Otherwise I would have to replicate tML's assembly loading process
            // (which includes modifying stuff using Cecil).
            //
            // I do realise that even having to do this means C# scripting isn't the best idea for this mod's functionality.
            // But now I'm so far down the rabbit hole that I have to make it work anyway.
            var tmlAssembly = typeof(ModLoader).Assembly;
            var assemblyManager = tmlAssembly.GetType("Terraria.ModLoader.Core.AssemblyManager");
            var assemblyBinariesField =
                assemblyManager.GetField("assemblyBinaries", BindingFlags.Static | BindingFlags.NonPublic);
            var assemblyBinaries =
                (IDictionary<string, byte[]>) assemblyBinariesField.GetValue(null);
            return assemblyBinaries[mod.Code.GetName().Name];
        }

        private Assembly CompileScript(CommandCaller caller, ScriptDefinition script)
        {
            _params.OutputAssembly = GetScriptAssemblyPath(script.Name);
            CompilerResults result;
            
            // The LD_PRELOAD environment variable will be passed to the compiler, which leads to errors if a library
            // happens to be in there that cannot be preloaded (namely, the steam overlay library, which is 32-bit and
            // cannot be preloaded into the 64-bit compiler).
            using (new CleanLdPreload())
            {
                // Actually compile script
                result = _cdp.CompileAssemblyFromSource(_params, script.Code);
            }

            // Store the output regardless of what happens, so compiler warnings etc. can also be inspected
            var outputArray = new string[result.Output.Count];
            result.Output.CopyTo(outputArray, 0);
            var output = string.Join("\n", outputArray);
            _compilationOutput[script.Name] = output;

            // Warn the user only if there were errors
            if (result.Errors.HasErrors)
            {
                _erroredScripts.Add(script.Name);
                caller.Reply("Compilation errors occurred while compiling script '" + script.Name +
                             "'! Do '/tcci viewOutput " + script.Name + "' to view output.");
                ModContent.GetInstance<TCCI>().Logger.Error($"Script compilation errors for {script.Name}.csx: {output}");
                return null;
            }

            Environment.UpdateScript(script.Name, script.Code);

            // Do this instead of just accessing the assembly from the CompilerResults because that assembly is cached
            // in some strange way.
            return Assembly.Load(File.ReadAllBytes(GetScriptAssemblyPath(script.Name)));
        }

        private string[] ScanScriptFolder()
        {
            return Directory.GetFiles(ScriptCodeFolderPath, "*.csx");
        }

        public static void EnsureFoldersExist()
        {
            EnsureFolderExists(ScriptCodeFolderPath);
            EnsureFolderExists(TCCIReferencesFolderPath);
            EnsureFolderExists(ScriptAssemblyFolderPath);
        }

        private static void EnsureFolderExists(string path)
        {
            if (!Directory.Exists(path))
            {
                if (File.Exists(path))
                    // path exists but is not a directory. Someone messed up.
                    throw new Exception("Path '" + path +
                                        "' required for TCCI exists but is not a directory! Please remove or rename the file at that path.");

                Directory.CreateDirectory(path);
            }
        }

        public void EnsureTemplateScriptExists()
        {
            var templatePath = GetScriptCodePath(TemplateScriptName);
            if (!File.Exists(templatePath)) File.WriteAllText(templatePath, DefaultTemplateScript.Text);
        }

        private string ReadScript(string scriptName)
        {
            var content = File.ReadAllText(GetScriptCodePath(scriptName));
            return _template.Replace(ScriptNamePlaceholder, scriptName).Replace(ScriptCodePlaceholder, content);
        }

        public bool IsScriptLoaded(string scriptName)
        {
            return LoadedScripts.Any(pair => pair.Value.Name == scriptName);
        }

        private static string GetScriptCodePath(string scriptName)
        {
            return Path.Combine(ScriptCodeFolderPath, scriptName + ".csx");
        }

        private static string GetScriptAssemblyPath(string scriptName)
        {
            return Path.Combine(ScriptAssemblyFolderPath, scriptName + ".dll");
        }

        private class ScriptDefinition
        {
            public ScriptCategory Category;
            public string Code;
            public string Name;
        }

        private class CleanLdPreload : IDisposable
        {
            private readonly string _ldPreload;
            private static readonly string LD_PRELOAD_NAME = "LD_PRELOAD";
            
            public CleanLdPreload()
            {
                _ldPreload = System.Environment.GetEnvironmentVariable(LD_PRELOAD_NAME);
                System.Environment.SetEnvironmentVariable(LD_PRELOAD_NAME, null);
            }

            public void Dispose()
            {
                System.Environment.SetEnvironmentVariable(LD_PRELOAD_NAME, _ldPreload);
            }
        }
    }
}