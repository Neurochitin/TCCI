namespace TCCI.Scripting
{
    public abstract class Script : UtilityMethods
    {
        public abstract void Run(ExecutionContext ctx);
    }
}