using TCCI.Data;

namespace TCCI.Scripting
{
    public class ExecutionContext
    {
        public ExecutionContext(EventInfo eventInfo, ScriptContainer script)
        {
            Script = script;
            Info = eventInfo;
        }

        private ScriptContainer Script { get; }
        public EventInfo Info { get; }

        public SubscriptionEventInfo SubInfo => (SubscriptionEventInfo) Info;
        public ResubEventInfo ResubInfo => (ResubEventInfo) Info;
        public DonationEventInfo DonationInfo => (DonationEventInfo) Info;
        public HostEventInfo HostInfo => (HostEventInfo) Info;
        public BitsEventInfo BitsInfo => (BitsEventInfo) Info;
        public RaidEventInfo RaidInfo => (RaidEventInfo) Info;
        public MerchEventInfo MerchInfo => (MerchEventInfo) Info;
        public PrimeSubGiftEventInfo PrimeSubGiftInfo => (PrimeSubGiftEventInfo) Info;
        public ChatCommandEventInfo CommandInfo => (ChatCommandEventInfo) Info;
    }
}