using System;
using System.Linq;
using Terraria.ModLoader;

namespace TCCI.Commands
{
    public abstract class AliasCommand : ModCommand
    {
        public override void Action(CommandCaller caller, string input, string[] args)
        {
            try
            {
                string[] newArgs = new[] {ReferencedAction}.Concat(args).ToArray();
                TCCICommand.ActionStatic(caller, input, newArgs);
            }
            catch (Exception e)
            {
                ModContent.GetInstance<TCCI>().Logger.Error("Error while executing AliasCommand: ", e);
            }
        }
        
        public override CommandType Type => CommandType.World | CommandType.Console;
        protected abstract string ReferencedAction { get; }
    }
    
    public class QuickReloadCommand : AliasCommand
    {
        public override string Command => "r";
        protected override string ReferencedAction => "reload";
    }

    public class QuickTestCommand : AliasCommand
    {
        private string _lastScriptName = "";
        
        public override void Action(CommandCaller caller, string input, string[] args)
        {
            if (args.Length > 0)
            {
                base.Action(caller, input, args);
                _lastScriptName = args[0];
                return;
            }
            
            Action(caller, input, new []{_lastScriptName});
        }

        public override string Command => "t";
        protected override string ReferencedAction => "testscript";
    }
}