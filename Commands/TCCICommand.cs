using System;
using System.Linq;
using System.Threading;
using TCCI.Data;
using TCCI.Network;
using TCCI.Scripting;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace TCCI.Commands
{
    public class TCCICommand : ModCommand
    {
        public override string Command => "TCCI";
        public override CommandType Type => CommandType.World | CommandType.Console;

        public override void Action(CommandCaller caller, string input, string[] args)
        {
            ActionStatic(caller, input, args);
        }

        public static void ActionStatic(CommandCaller caller, string input, string[] args)
        {
            if (!EnsureArgsCount(caller, args, 1)) return;

            var modInstance = ModContent.GetInstance<TCCI>();

            try
            {
                switch (args[0].ToLower())
                {
                    case "connect":
                        if (!EnsureArgsCount(caller, args, 2)) return;
                        modInstance.Connections.Connect(ParseType(caller, args[1]));
                        break;
                    case "disconnect":
                        if (!EnsureArgsCount(caller, args, 2)) return;
                        modInstance.Connections.Disconnect(ParseType(caller, args[1]));
                        break;
                    case "pause":
                        modInstance.IsPaused = true;
                        caller.Reply("Execution of actions paused. Resume with '/tcci resume'");
                        break;
                    case "resume":
                        modInstance.IsPaused = false;
                        caller.Reply("Resuming execution of actions.");
                        break;
                    case "viewoutput":
                        if (!EnsureArgsCount(caller, args, 2)) return;
                        caller.Reply("Output while compiling script '" + args[1] + "': " +
                                modInstance.ScriptLoader.GetCompilationOutput(args[1]));
                        break;
                    case "testscript":
                        if (!EnsureArgsCount(caller, args, 2)) return;
                        var script =
                            modInstance.ScriptLoader.LoadedScripts.Values.FirstOrDefault(container => container.Name == args[1]);
                        if (script == null) caller.Reply("Script does not exist! There might be no matching file, or perhaps you forgot to reload.");
                        else
                        {
                            EventInfo eventInfo = CreateTestEventInfo(caller, script, args.Length > 2 ? string.Join(" ", args.Skip(2)) : null);
                            modInstance.ScriptExecutor.Execute(script, eventInfo);
                        }
                        break;
                    case "reload":
                        CommandReload(caller, false);
                        break;
                    case "cleanreload":
                        CommandReload(caller, true);
                        break;
                    case "setcooldown":
                        if (!EnsureArgsCount(caller, args, 3)) return;
                        if (!modInstance.ScriptLoader.IsScriptLoaded(args[1]))
                            caller.Reply(
                                $"Warning: Script {args[1]} does not exist. Cooldown will be set anyway (it will apply to future scripts by that name)");
                        modInstance.ScriptExecutor.CooldownValueStore[args[1]] = int.Parse(args[2]);
                        break;
                    case "getcooldown":
                        if (!EnsureArgsCount(caller, args, 2)) return;
                        caller.Reply(
                            $"Script {args[1]} has a cooldown of {modInstance.ScriptExecutor.CooldownValueStore[args[1]]} seconds. " +
                            (modInstance.ScriptExecutor.CooldownTracker.IsScriptOnCooldown(args[1])
                                ? $"Time until next usage: {modInstance.ScriptExecutor.CooldownTracker.GetRemainingCooldownTime(args[1]) / 1000.0} seconds"
                                : "It is not currently on cooldown."));
                        break;
                    case "togglecooldowns":
                        modInstance.ScriptExecutor.CooldownTracker.CooldownsEnabled =
                            !modInstance.ScriptExecutor.CooldownTracker.CooldownsEnabled;
                        caller.Reply("Cooldowns are now " + (modInstance.ScriptExecutor.CooldownTracker.CooldownsEnabled
                            ? "enabled"
                            : "disabled") + ".");
                        break;
                    case "resetcooldown":
                        if (!EnsureArgsCount(caller, args, 2)) return;
                        modInstance.ScriptExecutor.CooldownTracker.ResetCooldown(args[1]);
                        caller.Reply("Cooldown of script " + args[1] + " has been reset.");
                        break;
                    case "replay":
                        if (modInstance.ScriptExecutor.LastExecutedScript == null) caller.Reply("No script to replay!");
                        else modInstance.ScriptExecutor.Execute(modInstance.ScriptExecutor.LastExecutedScript, modInstance.ScriptExecutor.LastEventInfo);
                        break;
                    case "targetme":
                        if (caller.Player == null)
                            caller.Reply("This command cannot be used in the server console!");
                        else
                        {
                            UtilityMethods.TargetedPlayer = caller.Player;
                            caller.Reply("Set targeted player to: " + caller.Player.name);
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                ModContent.GetInstance<TCCI>().Logger.Error("Error while executing command!", e);
                caller.Reply("Error while executing command: " + e);
            }

            // If we are the server, sync the (potentially changed) state to the client.
            if (Main.netMode == NetmodeID.Server) modInstance.SyncState();
        }

        private static EventInfo CreateTestEventInfo(CommandCaller caller, ScriptContainer script, string s)
        {
            Type eventInfoType = EventTypeUtil.GetEventInfoType(script.Category.Event);
            if (eventInfoType == null)
            {
                caller.Reply("Could not find event info type for event: " + script.Category.Event);
                return new EventInfo();
            }
            
            var eventInfo = (EventInfo) Activator.CreateInstance(eventInfoType);
            if (eventInfo == null)
            {
                caller.Reply("Could not instantiate event info type!");
                return new EventInfo();
            }

            if (s != null)
            {
                foreach (var param in s.Split(','))
                {
                    var data = param.Split('=');
                    if (data.Length == 2)
                    {
                        string fieldName = data[0].Trim(), value = data[1].Trim();
                        var field = eventInfoType.GetFields()
                            .FirstOrDefault(f => f.Name.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase));

                        if (field == null)
                        {
                            caller.Reply("Could not find field '" + fieldName + "'");
                            continue;
                        }

                        if (field.FieldType == typeof(int))
                        {
                            int intValue;
                            if (int.TryParse(value, out intValue)) field.SetValue(eventInfo, intValue);
                            else caller.Reply("Could not set field '" + fieldName + "' to value '" + value + "'");
                        } else if (field.FieldType == typeof(string))field.SetValue(eventInfo, value);
                        else caller.Reply("Cannot set field '" + fieldName + "' to value of type " + field.GetType() + " yet.");
                    }
                    else caller.Reply("Invalid format, extra data should be formatted as Amount=1000,Message=Hello");
                }
            }

            return eventInfo;
        }

        private static bool _reloading = false;
        private static Thread _reloadThread;

        private static void CommandReload(CommandCaller caller, bool clean)
        {
            if (_reloading)
            {
                caller.Reply("A reload is currently ongoing.");
                return;
            }

            _reloadThread = new Thread(() =>
            {
                try
                {
                    _reloading = true;
                    ModContent.GetInstance<TCCI>().ScriptLoader.ReloadScripts(caller, clean);
                    caller.Reply("Loaded " + ModContent.GetInstance<TCCI>().ScriptLoader.LoadedScripts.Count + " scripts.");
                    if (Main.netMode == NetmodeID.Server) ModContent.GetInstance<TCCI>().SyncState();
                }
                finally
                {
                    _reloading = false;
                }
            });
            _reloadThread.Start();
        }

        private static bool EnsureArgsCount(CommandCaller caller, string[] args, int count)
        {
            if (args.Length < count)
            {
                caller.Reply("Command needs at least " + count + " argument" + (count > 1 ? "s" : "") + ".");
                return false;
            }

            return true;
        }

        private static ConnectionType ParseType(CommandCaller caller, string typeStr)
        {
            var type = ConnectionType.None;

            switch (typeStr.ToLower())
            {
                case "chat":
                    type |= ConnectionType.Irc;
                    break;
                case "streamlabs":
                    type |= ConnectionType.StreamLabs;
                    break;
                case "all":
                    type = ConnectionType.Irc | ConnectionType.StreamLabs;
                    break;
                default:
                    caller.Reply("Invalid connection type.");
                    return ConnectionType.None;
            }

            return type;
        }
    }
}