using System.Collections.Generic;
using System.IO;
using TCCI.Scripting;

namespace TCCI.Data
{
    public class CooldownValueStore
    {
        private static readonly string FilePath = Path.Combine(ScriptLoader.TCCIReferencesFolderPath, "cooldowns.json");
        private Dictionary<string, long> _cooldowns;

        public long this[string scriptName]
        {
            get
            {
                Load();
                return _cooldowns.ContainsKey(scriptName) ? _cooldowns[scriptName] : -1;
            }
            set
            {
                if (_cooldowns == null) Load();
                _cooldowns[scriptName] = value;
                Save();
            }
        }

        public void Load()
        {
            _cooldowns = JsonUtil.LoadJsonDictionaryFromFile<string, long>(FilePath);
        }

        public void Save()
        {
            JsonUtil.WriteJsonDictionaryToFile(FilePath, _cooldowns);
        }
    }
}