using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace TCCI.Data
{
    public static class JsonUtil
    {
        public static Dictionary<K, V> LoadJsonDictionaryFromFile<K, V>(string path)
        {
            Dictionary<K, V> data;

            if (!File.Exists(path))
            {
                data = new Dictionary<K, V>();
            }
            else
            {
                var content = File.ReadAllText(path);
                data = JsonConvert.DeserializeObject<Dictionary<K, V>>(content);
            }

            return data;
        }

        public static void WriteJsonDictionaryToFile<K, V>(string path, Dictionary<K, V> data)
        {
            File.Delete(path);
            File.WriteAllText(path, JsonConvert.SerializeObject(data, Formatting.Indented));
        }
    }
}