using System;

namespace TCCI.Data
{
    public enum EventType
    {
        Follow,
        Subscription,
        Resub,
        Donation,
        Host,
        Bits,
        Raid,
        Merch,
        LoyaltyStoreRedemption,
        PrimeSubGift,
        ChatCommand,
        None
    }

    public static class EventTypeUtil
    {
        public static EventType Parse(string eventTypeStr)
        {
            switch (eventTypeStr)
            {
                case "follow":
                    return EventType.Follow;
                case "subscription":
                    return EventType.Subscription;
                case "resub":
                    return EventType.Resub;
                case "donation":
                    return EventType.Donation;
                case "host":
                    return EventType.Host;
                case "bits":
                    return EventType.Bits;
                case "raid":
                    return EventType.Raid;
                case "merch":
                    return EventType.Merch;
                case "loyalty_store_redemption":
                    return EventType.LoyaltyStoreRedemption;
                case "prime_sub_gift":
                    return EventType.PrimeSubGift;
                default:
                    return EventType.None;
            }
        }

        public static Type GetEventInfoType(EventType eventType)
        {
            switch (eventType)
            {
                case EventType.Follow:
                    return typeof(FollowEventInfo);
                case EventType.Subscription:
                    return typeof(SubscriptionEventInfo);
                case EventType.Resub:
                    return typeof(ResubEventInfo);
                case EventType.Donation:
                    return typeof(DonationEventInfo);
                case EventType.Host:
                    return typeof(HostEventInfo);
                case EventType.Bits:
                    return typeof(BitsEventInfo);
                case EventType.Raid:
                    return typeof(RaidEventInfo);
                case EventType.Merch:
                    return typeof(MerchEventInfo);
                case EventType.LoyaltyStoreRedemption:
                    return typeof(MerchEventInfo);
                case EventType.PrimeSubGift:
                    return typeof(PrimeSubGiftEventInfo);
                case EventType.ChatCommand:
                    return typeof(ChatCommandEventInfo);
            }
            
            return null;
        }
    }

    public class EventInfo
    {
        public bool IsTest;
        public string Name;
        public int Priority;

        public virtual string Parameter => "";
    }

    public class FollowEventInfo : EventInfo
    {
    }

    public class SubscriptionEventInfo : EventInfo
    {
        public string Message;
        public int Months;
        public string SubPlan;

        public override string Parameter => Months.ToString();
    }

    public class ResubEventInfo : SubscriptionEventInfo
    {
        public int Amount;
        public int StreakMonths;

        public override string Parameter => Amount.ToString();
    }

    public class DonationTarget
    {
        public string Name;
    }

    public class DonationEventInfo : EventInfo
    {
        public int Amount;
        public string Currency;
        public string FormattedAmount;
        public string From;
        public int FromUserId;
        public string Message;
        public DonationTarget To;

        public override string Parameter => Amount.ToString();
    }

    public class HostEventInfo : EventInfo
    {
        public int Viewers;

        public override string Parameter => Viewers.ToString();
    }

    public class BitsEventInfo : EventInfo
    {
        public int Amount;
        public string Currency;
        public string Message;

        public override string Parameter => Amount.ToString();
    }

    public class RaidEventInfo : EventInfo
    {
        public int Raiders;

        public override string Parameter => Raiders.ToString();
    }

    public class MerchEventInfo : EventInfo
    {
        public string Condition;
        public string From;
        public string ImageHref;
        public string Message;
        public string Product;
        public DonationTarget To;
    }

    // loyalty_store_redemption data is the same as merch data

    public class PrimeSubGiftEventInfo : EventInfo
    {
        public string From;
        public string GiftType;
        public string To;
    }

    public class ChatCommandEventInfo : EventInfo
    {
        public string Command;

        public override string Parameter => Command;
    }
}