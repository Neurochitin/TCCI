﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Microsoft.Xna.Framework;
using TCCI.Network;
using TCCI.Scripting;
using TCCI.UI;
using Terraria;
using Terraria.ID;
using Terraria.Localization;
using Terraria.ModLoader;
using Terraria.ModLoader.Config;
using Terraria.UI;

namespace TCCI
{
    public class TCCI : Mod
    {
        public bool IsPaused { get; set; } = false;
        public ScriptLoader ScriptLoader { get; } = new ScriptLoader();
        public ScriptExecutor ScriptExecutor { get; } = new ScriptExecutor();
        public Connections Connections { get; private set; }

        private ReloadProgressUI _rpUiState;
        private StatusUI _statusUiState;
        private UserInterface _rpUi, _statusUi;
        
        private NetworkSyncedState _networkState = null;
        
        public override void Load()
        {
            ScriptLoader.EnsureFoldersExist();
            ScriptLoader.EnsureTemplateScriptExists();
            Connections = new Connections();

            // Set properties required for secure connections.
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            Connections.Connected += ConnectionsOnConnected;
            Connections.Disconnected += ConnectionsOnDisconnected;
            Connections.ConnectionError += ConnectionsOnConnectionError;
            Connections.Event += ConnectionsOnEvent;

            if (!Main.dedServ)
            {
                _rpUiState = new ReloadProgressUI();
                _rpUiState.Activate();
                _rpUi = new UserInterface();
                _rpUi.SetState(_rpUiState);
                
                _statusUiState = new StatusUI();
                _statusUiState.Activate();
                _statusUi = new UserInterface();
                _statusUi.SetState(_statusUiState);
            }
        }

        public override void Unload()
        {
            Connections.Disconnect(ConnectionType.Irc | ConnectionType.StreamLabs);
        }

        public override void PreUpdateEntities()
        {
            UtilityMethods.ExecuteTickActions();
        }

        public override void ModifyInterfaceLayers(List<GameInterfaceLayer> layers)
        {
            base.ModifyInterfaceLayers(layers);
            
            int mouseTextIndex = layers.FindIndex(layer => layer.Name.Equals("Vanilla: Mouse Text"));
            if (mouseTextIndex != -1) {
                layers.Insert(mouseTextIndex, new LegacyGameInterfaceLayer(
                    "TCCI: Reload progress bar",
                    () =>
                    {
                        if (ReloadProgressUI.Visible) _rpUi.Draw(Main.spriteBatch, new GameTime());
                        return true;
                    },
                    InterfaceScaleType.UI)
                );
                layers.Insert(mouseTextIndex, new LegacyGameInterfaceLayer(
                    "TCCI: Status information",
                    () =>
                    {
                        if(StatusUI.Visible && !Main.playerInventory && ModContent.GetInstance<TCCIConfig>().ShowStatusUI) _statusUi.Draw(Main.spriteBatch, new GameTime());
                        return true;
                    },
                    InterfaceScaleType.UI)
                );
            }
        }

        public override void UpdateUI(GameTime gameTime)
        {
            _rpUi?.Update(gameTime);
            _statusUi?.Update(gameTime);
        }

        public override void HandlePacket(BinaryReader reader, int whoAmI)
        {
            byte type = reader.ReadByte();
            switch (type)
            {
                case 0:
                    // The server wants to sync its state with us.
                    HandleSync(reader);
                    break;
                default:
                    throw new Exception("Invalid packet type: " + type);
            }
        }

        private void HandleSync(BinaryReader reader)
        {
            if(_networkState == null) _networkState = new NetworkSyncedState();
            _networkState.ReadFrom(reader);
        }

        // Sync our state to the network. If we are the server, it will be sent to all clients by default, or the
        // specific client if specified. If we are a client, then this method shouldn't really be called, but it should
        // send it to the server.
        public void SyncState(int target = -1)
        {
            if(State == null) return;
            
            ModPacket packet = GetPacket();
            packet.Write((byte) 0); // packet type: sync
            NetworkSyncedState.Write(packet, State);
            packet.Send(target);
        }
        
        // Returns the state that should be used for UI operations, taking into account that we may be in multiplayer.
        // This may be null!
        public IState State
        {
            get
            {
                switch (Main.netMode)
                {
                    case NetmodeID.Server:
                    case NetmodeID.SinglePlayer:
                        return new LocallyReferentialState(this);
                    case NetmodeID.MultiplayerClient:
                        return _networkState;
                    default:
                        return null;
                }
            }
        }

        private void ConnectionsOnEvent(object sender, StreamEventArgs e)
        {
            if (IsPaused) return;
            var scripts = ScriptLoader.GetLoadedScriptsFor(e.Type, e.Info.Parameter);
            foreach (var script in scripts) ScriptExecutor.Execute(script, e.Info);
        }

        private void ConnectionsOnConnectionError(object sender, ConnectionErrorEventArgs e)
        {
            Message("Error in connection " + e.Type.ToStringIndividual() + ": " + e.Message);
        }

        private void ConnectionsOnDisconnected(object sender, ConnectionEventArgs e)
        {
            var str = e.Type.ToStringIndividual();

            Message("Disconnected from " + str + ". Use '/tcci connect " + str.ToLower() + "' to reconnect.");
            if (Main.netMode == NetmodeID.Server) ModContent.GetInstance<TCCI>().SyncState();
        }

        private void ConnectionsOnConnected
            (object sender, ConnectionEventArgs e)
        {
            Message("Connected to " + e.Type.ToStringIndividual());
            if (Main.netMode == NetmodeID.Server) ModContent.GetInstance<TCCI>().SyncState();
        }

        public static void Message(string text)
        {
            Message(text, Color.White);
        }

        public static void Message(string text, Color colour)
        {
            var chatText = "[TCCI] " + text;
            switch (Main.netMode)
            {
                case NetmodeID.SinglePlayer:
                case NetmodeID.MultiplayerClient:
                    Main.NewText(chatText, colour);
                    break;
                default: // server
                    Console.WriteLine(chatText);
                    // TODO: i18n
                    NetMessage.BroadcastChatMessage(NetworkText.FromLiteral(chatText), colour);
                    break;
            }

            TCCI mod = ModContent.GetInstance<TCCI>();
            if (mod != null) mod.Logger?.Info(text);
            else Console.WriteLine("[TCCI Message] " + text);
        }
    }

    public class TCCIPlayer : ModPlayer
    {
        public override void SyncPlayer(int toWhom, int fromWhom, bool newPlayer)
        {
            if(Main.netMode == NetmodeID.Server)
            {
                // The player fromWhom has joined.
                ModContent.GetInstance<TCCI>().SyncState(fromWhom);
            }
        }
    }
}