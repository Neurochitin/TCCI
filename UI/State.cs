using System;
using System.IO;
using TCCI.Network;

namespace TCCI.UI
{
    public interface IState
    {
        int LoadedScriptCount { get; }
        ConnectionType ActiveConnections { get; }
        bool IsPaused { get; }
    }

    // Delegates properties to a local instance of the TCCI class. (Client side)
    public class LocallyReferentialState : IState
    {
        private readonly TCCI _mod;

        public LocallyReferentialState(TCCI mod)
        {
            _mod = mod;
        }

        public int LoadedScriptCount => _mod.ScriptLoader.LoadedScripts.Count;
        public ConnectionType ActiveConnections => _mod.Connections.ActiveConnections;
        public bool IsPaused => _mod.IsPaused;
    }

    // Stores properties that will be synced on occasion.
    public class NetworkSyncedState : IState
    {
        public int LoadedScriptCount { get; private set; }
        public ConnectionType ActiveConnections { get; private set; }
        public bool IsPaused { get; private set; }

        // Reads values from the given reader
        public void ReadFrom(BinaryReader reader)
        {
            LoadedScriptCount = reader.ReadInt32();
            ActiveConnections = (ConnectionType) reader.ReadInt32();
            IsPaused = reader.ReadBoolean();
        }

        // Writes an arbitrary state to the given writer
        public static void Write(BinaryWriter writer, IState state)
        {
            writer.Write(state.LoadedScriptCount);
            writer.Write((int) state.ActiveConnections);
            writer.Write(state.IsPaused);
        }
    }
}