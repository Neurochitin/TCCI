using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.GameContent.UI.Elements;
using Terraria.UI;

namespace TCCI.UI
{
    public class ReloadProgressUI : UIState
    {
        private CustomProgressBar _progressBar;
        private UIPanel _panel;
        private UITextPanel<string> _textPanel;
        public static bool Visible;
        public static int NumScripts;
        public static int CurrentScript;
        public static string CurrentScriptName;

        public override void OnInitialize()
        {
            _panel = new UIPanel();
            _panel.Left.Set(450f, 0f);
            _panel.Top.Set(30f, 0f);
            _panel.Width.Set(300f, 0f);
            _panel.Height.Set(78f, 0f);
            _panel.SetPadding(5);
            _panel.BackgroundColor = new Color(0, 0, 70);

            _progressBar = new CustomProgressBar();
            _progressBar.Height.Percent = 1f;
            _progressBar.Width.Percent = 1f;
            _panel.Append(_progressBar);
            
            _textPanel = new UITextPanel<string>("0/0");
            _textPanel.Height.Percent = 1f;
            _textPanel.Width.Percent = 1f;
            _textPanel.BorderColor = Color.Transparent;
            _textPanel.BackgroundColor = Color.Transparent;
            _panel.Append(_textPanel);
            
            Append(_panel);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (Visible)
            {
                float progress = CurrentScript / (float) NumScripts;
                _progressBar.SetProgress(progress);
                _textPanel.SetText($"{CurrentScript} / {NumScripts}\n{CurrentScriptName}");
            }
        }

        public static void Reset(int numScripts)
        {
            NumScripts = numScripts;
            CurrentScript = 0;
            Visible = true;
        }
    }

    public class CustomProgressBar : UIElement
    {
        private CustomInnerProgressBar _progressBar = new CustomInnerProgressBar();
        private float _visualProgress;
        private float _targetProgress;

        public CustomProgressBar()
        {
            _progressBar.Height.Precent = 1f;
            _progressBar.Recalculate();
            Append(_progressBar);
        }

        public void SetProgress(float value)
        {
            _targetProgress = value;
            if (value >= _visualProgress)
                return;
            _visualProgress = value;
        }

        protected override void DrawSelf(SpriteBatch spriteBatch)
        {
            _visualProgress = (float) (_visualProgress * 0.95 + 0.05 * _targetProgress);
            _progressBar.Width.Precent = _visualProgress;
            _progressBar.Recalculate();
        }

        private class CustomInnerProgressBar : UIElement
        {
            protected override void DrawSelf(SpriteBatch spriteBatch)
            {
                CalculatedStyle dimensions = GetDimensions();
                spriteBatch.Draw(Main.magicPixel,
                    new Vector2(dimensions.X, dimensions.Y), new Rectangle?(),
                    Color.Blue,
                    0.0f, Vector2.Zero,
                    new Vector2(dimensions.Width, dimensions.Height / 1000f),
                    SpriteEffects.None, 0.0f);
            }
        }
    }
}