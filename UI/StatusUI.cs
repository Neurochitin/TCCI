using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TCCI.Network;
using Terraria;
using Terraria.GameContent.UI.Elements;
using Terraria.ModLoader;
using Terraria.UI;

namespace TCCI.UI
{
    public class StatusUI : UIState
    {
        private UIPanel _panel;
        private UIText _text;
        private ColoredHoverImage _emblem;
        public static bool Visible = true;

        private static readonly Color LightRed = new Color(255, 75, 75),
            LightGreen = new Color(75, 255, 75),
            LightYellow = new Color(255, 160, 75);

        private static readonly Rectangle Frame = new Rectangle(0, 0, 50, 40);
        private static Texture2D EmblemTexture => ModContent.GetInstance<TCCI>().GetTexture("UI/Emblem");

        public override void OnInitialize()
        {
            try
            {
                _panel = new UIPanel();
                _panel.Left.Set(21f, 0f);
                _panel.Top.Set(130f, 0f);
                _panel.Width.Set(300f, 0f);
                _panel.Height.Set(78f, 0f);

                _panel.BackgroundColor = Color.Transparent;
                _panel.BorderColor = Color.Transparent;

                _text = new UIText("TCCI status unknown");
                _text.Top.Pixels = 10;
                _text.Left.Pixels = 47;
                _text.TextColor = LightRed;

                if (EmblemTexture != null)
                {
                    ModContent.GetInstance<TCCI>().Logger.Info("Early initialised StatusUI");
                    _emblem = new ColoredHoverImage(EmblemTexture, Frame, "TCCI");
                }
                else _emblem = new ColoredHoverImage("TCCI");

                _emblem.Color = LightRed;
                _emblem.Top.Pixels = 0;

                _panel.Append(_text);
                _panel.Append(_emblem);

                Append(_panel);
            }
            catch (Exception e)
            {
                ModContent.GetInstance<TCCI>().Logger.Error("Error while initialising StatusUI: ", e);
            }
        }

        public override void Update(GameTime gameTime)
        {
            try
            {
                base.Update(gameTime);
                if (!Visible) return;
                
                // Check whether the panel is uninitialised; if it is, try to initialise
                if (!_emblem.Initialised && EmblemTexture != null)
                {
                    ModContent.GetInstance<TCCI>().Logger.Info("Late initialised StatusUI");
                    _emblem.SetImage(EmblemTexture, Frame);
                }

                // Shift the panel so it is always just below the buffs
                int numBuffs = Main.player[Main.myPlayer].CountBuffs();
                _panel.Top.Pixels = 70 + 51 * ((numBuffs + 10) / 11);

                // Fill the panel with content
                TCCI tcci = ModContent.GetInstance<TCCI>();
                IState state = tcci.State;

                List<string> successMessages = new List<string>(), failureMessages = new List<string>();

                if (state == null)
                {
                    failureMessages.Add(
                        "No TCCI status available (if you are on multiplayer, is TCCI installed on the server?");
                }
                else
                {
                    if (state.IsPaused) failureMessages.Add("Script execution is paused");
                    // Otherwise don't bother

                    if (state.LoadedScriptCount == 0) failureMessages.Add("0 scripts loaded");
                    else successMessages.Add(state.LoadedScriptCount + " scripts loaded");

                    switch (state.ActiveConnections)
                    {
                        case ConnectionType.None:
                            failureMessages.Add("No services connected");
                            break;
                        case ConnectionType.Irc:
                            successMessages.Add("Connected to chat");
                            break;
                        case ConnectionType.StreamLabs:
                            successMessages.Add("Connected to StreamLabs");
                            break;
                        case ConnectionType.Irc | ConnectionType.StreamLabs:
                            successMessages.Add("Connected to chat and StreamLabs");
                            break;
                    }
                }

                if (failureMessages.Count >= 2) _emblem.Color = LightRed;
                else if (failureMessages.Count == 1) _emblem.Color = LightYellow;
                else _emblem.Color = LightGreen;

                _text.SetText(string.Join(", ", failureMessages));
                _emblem.HoverText =
                    successMessages.Count == 0 ? "TCCI" : "TCCI: " + string.Join(", ", successMessages);
            }
            catch (Exception e)
            {
                ModContent.GetInstance<TCCI>().Logger.Error("Error while updating StatusUI: ", e);
            }
        }
    }

    public class ColoredHoverImage : UIImageFramed
    {
        public string HoverText { get; set; }

        private bool _initialised;

        public ColoredHoverImage(string hoverText) : base(null, Rectangle.Empty)
        {
            HoverText = hoverText;
        }

        public ColoredHoverImage(Texture2D texture, Rectangle frame, string hoverText) : base(texture, frame)
        {
            _initialised = true;
            HoverText = hoverText;
        }

        public new void SetImage(Texture2D texture, Rectangle frame)
        {
            base.SetImage(texture, frame);
            _initialised = true;
        }

        public bool Initialised => _initialised;

        protected override void DrawSelf(SpriteBatch spriteBatch)
        {
            try
            {
                // Only draw the base if we are initialised (i.e. have a texture set). Otherwise this will lead to
                // begin/end errors
                if(_initialised) base.DrawSelf(spriteBatch);

                CalculatedStyle style = GetDimensions();
                if (style.X < Main.mouseX && style.X + style.Width > Main.mouseX && style.Y < Main.mouseY &&
                    style.Y + style.Height > Main.mouseY)
                    Main.hoverItemName = HoverText;
            }
            catch (Exception e)
            {
                ModContent.GetInstance<TCCI>().Logger.Error("Error while drawing StatusUI: ", e);
            }
        }
    }
}